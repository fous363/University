package kz.zhanbolat.university.student.boundary;

import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.group.boundary.GroupService;
import kz.zhanbolat.university.group.boundary.dto.GroupForm;
import kz.zhanbolat.university.student.boundary.dto.StudentForm;
import kz.zhanbolat.university.student.boundary.mapper.StudentMapper;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;

    @ModelAttribute("student")
    public StudentForm student() {
        return new StudentForm();
    }

    @GetMapping("/singUp")
    public String studentSingUp() {
        return "studentSingUp";
    }

    @PostMapping("/singUp")
    public String signUp(@ModelAttribute("student") StudentForm studentDto, HttpSession session) {
        String username = (String) session.getAttribute("username");
        Student student = StudentMapper.map(studentDto);
        User user = userService.findUserByUsername(username);
        student.setUser(user);
        studentService.registerNewStudent(student);
        session.removeAttribute("username");
        return "redirect:/user/login";
    }

    @GetMapping("/profile")
    public String profilePage(ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        Student student = studentService.getStudentByUsername(username);
        model.addAttribute("student", StudentMapper.map(student));
        return "studentProfile";
    }

    @GetMapping("/profile/update")
    public String profileUpdatePage(ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        Student student = studentService.getStudentByUsername(username);
        model.addAttribute("student", StudentMapper.map(student));
        return "studentProfileUpdate";
    }

    @PostMapping("/profile/update")
    public String processProfileUpdate(@ModelAttribute("student") StudentForm student, HttpSession session) {
        studentService.updateStudentInfo(StudentMapper.map(student),
                UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token")));
        return "redirect:/student/profile";
    }

    @GetMapping("/admin")
    public String adminStudentPage(ModelMap model) {
        model.addAttribute("students",
                studentService.getAllStudents().stream().map(StudentMapper::map).collect(Collectors.toList()));
        return "adminStudents";
    }

    @GetMapping("/admin/assign/{student_id}")
    public String adminAssignStudentToGroupPage(@PathVariable("student_id") Integer studentId, ModelMap model) {
        model.addAttribute("student", StudentMapper.map(studentService.getStudent(studentId)));
        model.addAttribute("groups", groupService.getAllGroups());
        model.addAttribute("group", new GroupForm());
        return "adminAssignStudentToGroup";
    }

    @PostMapping("/admin/assign/{student_id}")
    public String processAdminAssignStudentToGroup(@PathVariable("student_id")Integer studentId,
                                                   @ModelAttribute("group")GroupForm group) {
        studentService.assignStudentToGroup(studentService.getStudent(studentId), groupService.getGroup(group.getCode()));
        return "redirect:/student/admin";
    }


    @GetMapping("/lecturer/mark/{schedule_id}")
    public String lecturerStudentsMark(@PathVariable("schedule_id")Integer scheduleId, ModelMap model) {
        List<Student> students = studentService.getAllStudentsBySchedule(scheduleId);
        model.addAttribute("students", students);
        model.addAttribute("schedule_id", scheduleId);
        return "lecturerStudentsMark";
    }
}
