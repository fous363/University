package kz.zhanbolat.university.log.control;

import kz.zhanbolat.university.log.entity.Log;

import java.util.List;

public interface LogRepository {
    void create(Log log);
    List<Log> findAll();
}
