package kz.zhanbolat.university.user.control.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;

@Component
public class MessageEncryptor {
    @Autowired
    private MessageDigest messageDigest;

    public String encryptMessage(String message) {
        return convertToString(messageDigest.digest(message.getBytes()));
    }

    private String convertToString(byte[] digestMessage) {
        StringBuilder builder = new StringBuilder();
        for (byte b : digestMessage) {
            builder.append(String.format("%02x", Byte.toUnsignedInt(b)));
        }
        return builder.toString();
    }
}
