package kz.zhanbolat.university.student.boundary.mapper;

import kz.zhanbolat.university.student.boundary.dto.StudentForm;
import kz.zhanbolat.university.student.entity.Student;

import java.util.Objects;

public class StudentMapper {

    public static Student map(StudentForm source) {
        return Student.builder().id(source.getId())
                                .fio(source.getFio())
                                .birthday(source.getBirthday())
                                .address(source.getAddress())
                                .phone(source.getPhone())
                                .email(source.getEmail())
                                .build();
    }

    public static StudentForm map(Student source) {
        StudentForm destination =  StudentForm.builder()
                .id(source.getId())
                .fio(source.getFio())
                .birthday(source.getBirthday())
                .address(source.getAddress())
                .email(source.getEmail())
                .phone(source.getPhone())
                .build();
        if (Objects.nonNull(source.getGroup())) {
            destination.setGroup(source.getGroup().getCode());
        }
        return destination;
    }
}
