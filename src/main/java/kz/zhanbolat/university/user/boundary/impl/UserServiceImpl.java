package kz.zhanbolat.university.user.boundary.impl;

import kz.zhanbolat.university.config.exception.InternalServerException;
import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.user.control.UserRepository;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.control.UserRestrictions;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import kz.zhanbolat.university.user.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional(rollbackOn = RuntimeException.class)
public class UserServiceImpl implements UserService {
    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageEncryptor encryptor;

    @Override
    public boolean authenticateUser(String login, String password) {
        return userRepository.checkUserExists(login, encryptor.encryptMessage(password));
    }

    @Override
    public void registerNewUser(User user) {
        String username = user.getUsername();
        String password = user.getPassword();
        checkCredentials(username, password);
        String encryptedPassword = encryptor.encryptMessage(password);
        if (userRepository.checkUserExists(username, encryptedPassword)) {
            throw new IllegalArgumentException("User already exists");
        }
        user.setPassword(encryptedPassword);
        try {
            userRepository.create(user);
        } catch (Exception e) {
            logger.error("Error on user creating {e}", e);
            throw new InternalServerException();
        }
    }

    @Override
    public User findUserByUsername(String username) {
        checkCredentials(username);
        return userRepository.findOneByUsername(username);
    }

    @Override
    public String getUserAuthority(String username) {
        checkCredentials(username);
        return userRepository.findAuthorityByUsername(username);
    }

    @Override
    public void changePassword(String username, String password) {
        checkCredentials(username, password);
        String encryptedPassword = encryptor.encryptMessage(password);
        userRepository.updatePassword(username, encryptedPassword);
    }

    @Override
    public void updateUserAuthority(String username, String authority) {
        userRepository.updateUserAuthority(username, authority);
    }

    @Override
    public List<User> getAllUsersWithRestriction(UserRestrictions exclude, List<User> users) {
        if (exclude == UserRestrictions.EXCLUDE_ONE) {
            return userRepository.findAllWithExcludeOne(users.get(0));
        }
        throw new IllegalStateException();
    }

    @Override
    public void deleteUser(String username) {
        userRepository.deleteByUsername(username);
    }

    @Override
    public void activateUser(String username) {
        userRepository.updateUserActiveStatus(username, true);
    }

    @Override
    public void deactivateUser(String username) {
        userRepository.updateUserActiveStatus(username, false);
    }

    @Override
    public User getUserByStudent(Integer studentId) {
        return userRepository.findOneByStudentId(studentId);
    }

    private void checkCredentials(String... credentials) {
        for (var credential : credentials) {
            if (credential.isEmpty()) {
                throw new IllegalArgumentException("One of the argument should not be empty.");
            }
        }
    }
}
