package kz.zhanbolat.university.degree.boundary;

import kz.zhanbolat.university.degree.entity.AcademicDegree;

import java.util.List;

public interface AcademicDegreeService {
    List<AcademicDegree> getAllAcademicDegrees();
    AcademicDegree getAcademicDegree(String name);
}
