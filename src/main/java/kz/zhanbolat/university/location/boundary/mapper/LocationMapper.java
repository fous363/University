package kz.zhanbolat.university.location.boundary.mapper;

import kz.zhanbolat.university.location.boundary.dto.LocationForm;
import kz.zhanbolat.university.location.entity.Location;

public class LocationMapper {

    public static LocationForm map(Location source) {
        return new LocationForm(source.getId(), source.getAudience(), source.getCampus());
    }

    public static Location map(LocationForm source) {
        Location destination = new Location();
        destination.setId(source.getId());
        destination.setAudience(source.getAudience());
        destination.setCampus(source.getCampus());
        return destination;
    }
}
