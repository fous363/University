package kz.zhanbolat.university.log.boundary.view;

import kz.zhanbolat.university.log.boundary.dto.LogExportForm;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class LogXlsxView extends AbstractXlsView {
    private static final DateTimeFormatter OUTPUT_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd_MM_yyyy_hh_mm");

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fileName = new StringBuilder("logs_").append(LocalDateTime.now().format(OUTPUT_DATE_TIME_FORMATTER)).append(".xls").toString();

        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        List<LogExportForm> logs = (List<LogExportForm>) model.get("logs");
        Sheet sheet = workbook.createSheet("Logs");
        AtomicInteger rowCount = new AtomicInteger(0);
        Row header = sheet.createRow(rowCount.getAndIncrement());
        header.createCell(0).setCellValue("Username");
        header.createCell(1).setCellValue("Timestamp");
        header.createCell(2).setCellValue("Logged In");
        header.createCell(3).setCellValue("Logged Off");

        logs.forEach(log -> {
            Row row = sheet.createRow(rowCount.getAndIncrement());
            row.createCell(0).setCellValue(log.getUsername());
            row.createCell(1).setCellValue(log.getTimestamp());
            row.createCell(2).setCellValue(log.isLoggedIn());
            row.createCell(3).setCellValue(log.isLoggedOff());
        });
    }
}
