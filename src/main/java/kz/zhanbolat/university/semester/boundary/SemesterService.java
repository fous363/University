package kz.zhanbolat.university.semester.boundary;

import kz.zhanbolat.university.semester.entity.Semester;

import java.util.List;

public interface SemesterService {
    List<Semester> getAllStudentSemesters(Integer studentId);
    void createSemester(Semester semester);
    void deleteSemester(Integer semesterId);
    List<Semester> getAllSemesters();
    void updateSemester(Semester semester);
    Semester getSemester(Integer semesterId);
}
