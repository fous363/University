create table course_semester(
    course_id int not null,
    semester_id int not null,
    foreign key (course_id) references course(id),
    foreign key (semester_id) references semester(id)
);