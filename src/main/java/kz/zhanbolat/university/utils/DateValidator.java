package kz.zhanbolat.university.utils;

import kz.zhanbolat.university.semester.entity.Semester;

public class DateValidator {

    public static boolean validateSemesterDates(Semester semester) {
        return semester.getStartDate().isBefore(semester.getEndDate())
                && semester.getStartDate().getYear() == semester.getYear()
                && semester.getEndDate().getYear() == semester.getYear();
    }
}
