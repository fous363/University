package kz.zhanbolat.university.degree.entity;

import kz.zhanbolat.university.lecturer.entity.Lecturer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "academic_degree")
public class AcademicDegree {
    @Id
    private String name;
    private Integer priority;

    @OneToMany(mappedBy = "academicDegree", targetEntity = Lecturer.class)
    private List<Lecturer> lecturers;

    public AcademicDegree() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("priority", priority)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AcademicDegree that = (AcademicDegree) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(priority, that.priority)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(priority)
                .toHashCode();
    }
}
