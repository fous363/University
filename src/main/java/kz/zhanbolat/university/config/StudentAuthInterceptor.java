package kz.zhanbolat.university.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

public class StudentAuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = (String) request.getSession().getAttribute("access-token");
        String authorityName = UserAuthorityTable.INSTANCE.getAuthorityName(token);
        return Objects.equals("USER_STUDENT", authorityName) || Objects.equals("USER_ADMIN", authorityName)
                || Objects.equals("USER_LECTURER", authorityName);
    }
}
