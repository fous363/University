package kz.zhanbolat.university.semester.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class SemesterForm {
    private Integer id;
    private Integer number;
    private String name;
    private Integer year;
    private String startDate;
    private String endDate;

    public SemesterForm() {
    }

    public SemesterForm(Builder builder) {
        this.id = builder.id;
        this.number = builder.number;
        this.name = builder.name;
        this.year = builder.year;
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("number", number)
                .append("name", name)
                .append("year", year)
                .append("startDate", startDate)
                .append("endDate", endDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SemesterForm that = (SemesterForm) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(number, that.number)
                .append(name, that.name)
                .append(year, that.year)
                .append(startDate, that.startDate)
                .append(endDate, that.endDate)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(number)
                .append(name)
                .append(year)
                .append(startDate)
                .append(endDate)
                .toHashCode();
    }

    public static class Builder {
        private Integer id;
        private Integer number;
        private String name;
        private Integer year;
        private String startDate;
        private String endDate;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder number(Integer number) {
            this.number = number;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder year(Integer year) {
            this.year = year;

            return this;
        }

        public Builder startDate(String startDate) {
            this.startDate = startDate;

            return this;
        }

        public Builder endDate(String endDate) {
            this.endDate = endDate;

            return this;
        }

        public SemesterForm build() {
            return new SemesterForm(this);
        }
    }
}
