package kz.zhanbolat.university;

import kz.zhanbolat.university.log.control.LogRepository;
import kz.zhanbolat.university.log.boundary.impl.LogServiceImpl;
import kz.zhanbolat.university.log.entity.Log;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.control.UserRepository;
import kz.zhanbolat.university.user.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogServiceTest {
    @InjectMocks
    private LogServiceImpl logService;
    @Mock
    private UserService userService;
    @Mock
    private LogRepository logRepository;

    @Test
    public void createLogOfLogInShouldCreateLog() {
        when(userService.findUserByUsername(anyString())).thenReturn(User.builder().build());
        doNothing().when(logRepository).create(any(Log.class));

        logService.createLogOfLogIn("login");
    }

    @Test
    public void createLogOfLogInShouldThrowExceptionOnEmptyLogin() {
        when(userService.findUserByUsername("")).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> logService.createLogOfLogIn(""));
    }

    @Test
    public void createLogOfLogOutShouldCreateLog() {
        when(userService.findUserByUsername(anyString())).thenReturn(User.builder().build());
        doNothing().when(logRepository).create(any(Log.class));

        logService.createLogOfLogOut("login");
    }

    @Test
    public void createLogOfLogOutShouldThrowExceptionOnEmptyLogin() {
        when(userService.findUserByUsername("")).thenThrow(IllegalArgumentException.class);
        assertThrows(IllegalArgumentException.class, () -> logService.createLogOfLogOut(""));
    }
}
