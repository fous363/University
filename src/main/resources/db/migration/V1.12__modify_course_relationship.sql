drop table user_course;

alter table classes_schedule
drop constraint classes_schedule_course_code_fkey;

alter table classes_schedule
drop column course_code;

alter table course
drop constraint course_pkey;

alter table course
add id serial primary key;

alter table classes_schedule
add course_id int not null;

alter table classes_schedule
add foreign key (course_id) references course(id);

create table student_course(
    student_id int not null,
    course_id int not null,
    foreign key (student_id) references student(id),
    foreign key (course_id) references course(id)
);

create table lecturer_coures(
    lecturer_id int not null,
    course_id int not null,
    foreign key (lecturer_id) references lecturer(id),
    foreign key (course_id) references course(id)
);
