package kz.zhanbolat.university.user.control;

public interface TokenGenerator {
    String generateToken(String... credentials);
}
