package kz.zhanbolat.university.lecturer.boundary.impl;

import kz.zhanbolat.university.lecturer.boundary.LecturerService;
import kz.zhanbolat.university.lecturer.control.LecturerRepository;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@Transactional
public class LecturerServiceImpl implements LecturerService {
    @Autowired
    private LecturerRepository lecturerRepository;

    @Override
    public void registerNewLecturer(Lecturer lecturer) {
        if (Objects.isNull(lecturer.getBirthday())) {
            throw new IllegalArgumentException();
        }
        lecturerRepository.create(lecturer);
    }

    @Override
    public Lecturer getLecturerByUsername(String username) {
        return lecturerRepository.findOneByUsername(username);
    }

    @Override
    public void updateLecturerInfo(Lecturer lecturer, String username) {
        if (Objects.isNull(lecturer.getBirthday())) {
            throw new IllegalArgumentException();
        }
        lecturerRepository.updateLecturerInfoByUsername(lecturer, username);
    }
}
