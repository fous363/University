package kz.zhanbolat.university.schedule.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ClassesScheduleForm {
    private Integer id;
    private String dateTime;
    private String courseName;
    private String location;

    public ClassesScheduleForm() {
    }

    public ClassesScheduleForm(Builder builder) {
        this.id = builder.id;
        this.dateTime = builder.dateTime;
        this.courseName = builder.courseName;
        this.location = builder.location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String dateTime;
        private String courseName;
        private String location;

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder dateTime(String dateTime) {
            this.dateTime = dateTime;

            return this;
        }

        public Builder courseName(String courseName) {
            this.courseName = courseName;

            return this;
        }

        public Builder location(String location) {
            this.location = location;

            return this;
        }

        public ClassesScheduleForm build() {
            return new ClassesScheduleForm(this);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("dateTime", dateTime)
                .append("courseName", courseName)
                .append("location", location)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ClassesScheduleForm that = (ClassesScheduleForm) o;

        return new EqualsBuilder()
                .append(dateTime, that.dateTime)
                .append(courseName, that.courseName)
                .append(location, that.location)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(dateTime)
                .append(courseName)
                .append(location)
                .toHashCode();
    }
}

