package kz.zhanbolat.university.group.control.impl;

import kz.zhanbolat.university.group.control.GroupRepository;
import kz.zhanbolat.university.group.entity.Group;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class GroupRepositoryImpl implements GroupRepository {
    private static final String SELECT_ALL = "select id, code, name from student_group";
    private static final String COUNT_GROUP_BY_ID = "select count(*) from student_group where id=:group_id";
    private static final String DELETE_BY_ID = "delete from student_group where id=:group_id";
    private static final String SELECT_ONE_BY_CODE = "select id, code, name from student_group where code=:group_code";
    private static final String SELECT_ALL_BY_LECTURER_ID = "select student_group.id, student_group.code, " +
            "student_group.name from student_group " +
            "inner join student on student.group_id = student_group.id " +
            "inner join lecturer on lecturer.id = :lecturer_id " +
            "inner join user_course on lecturer.user_id = user_course.user_id";
    private static final String SELECT_COUNT_BY_CODE = "select count(*) from student_group where code = :code";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Group> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, Group.class);
        return query.getResultList();
    }

    @Override
    public Group findOne(Integer groupId) {
        return entityManager.find(Group.class, groupId);
    }

    @Override
    public void create(Group group) {
        entityManager.persist(group);
    }

    @Override
    public boolean isExisted(Integer id) {
        Query query = entityManager.createNativeQuery(COUNT_GROUP_BY_ID, Integer.class);
        query.setParameter("group_id", id);
        Integer count = (Integer) query.getSingleResult();
        return count > 0;
    }

    @Override
    public void update(Group group) {
        entityManager.merge(group);
    }

    @Override
    public void delete(Integer groupId) {
        Query query = entityManager.createNativeQuery(DELETE_BY_ID);
        query.setParameter("group_id", groupId);
        query.executeUpdate();
    }

    @Override
    public Group findOneByCode(String groupCode) {
        Query query = entityManager.createNativeQuery(SELECT_ONE_BY_CODE, Group.class);
        query.setParameter("group_code", groupCode);
        return (Group) query.getSingleResult();
    }

    @Override
    public List<Group> findAllByLecturerId(Integer lecturerId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_LECTURER_ID, Group.class);
        query.setParameter("lecturer_id", lecturerId);
        return query.getResultList();
    }

    @Override
    public boolean checkExists(Group group) {
        final Query query = entityManager.createNativeQuery(SELECT_COUNT_BY_CODE);
        query.setParameter("code", group.getCode());
        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
}
