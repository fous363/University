package kz.zhanbolat.university.log.entity;

public enum LogStatus {
    LOG_IN(true, false), LOG_OUT(false, true);

    private boolean loggedIn;
    private boolean loggedOff;

    LogStatus(boolean loggedIn, boolean loggedOff) {
        this.loggedIn = loggedIn;
        this.loggedOff = loggedOff;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public boolean isLoggedOff() {
        return loggedOff;
    }
}
