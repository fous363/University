package kz.zhanbolat.university.user.boundary;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.user.control.UserRestrictions;
import kz.zhanbolat.university.user.entity.User;

import java.util.List;

public interface UserService {
    boolean authenticateUser(String username, String password);
    void registerNewUser(User user);
    User findUserByUsername(String username);
    String getUserAuthority(String username);
    void changePassword(String username, String password);
    void updateUserAuthority(String username, String authority);
    List<User> getAllUsersWithRestriction(UserRestrictions exclude, List<User> users);
    void deleteUser(String username);
    void activateUser(String username);
    void deactivateUser(String username);
    User getUserByStudent(Integer studentId);
}
