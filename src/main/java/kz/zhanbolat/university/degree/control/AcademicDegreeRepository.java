package kz.zhanbolat.university.degree.control;

import kz.zhanbolat.university.degree.entity.AcademicDegree;

import java.util.List;

public interface AcademicDegreeRepository {
    List<AcademicDegree> findAll();
    AcademicDegree findOne(String name);
}
