package kz.zhanbolat.university;

import kz.zhanbolat.university.config.EncryptionConfig;
import kz.zhanbolat.university.config.TestDBConfiguration;
import kz.zhanbolat.university.lecturer.boundary.impl.LecturerServiceImpl;
import kz.zhanbolat.university.lecturer.control.impl.LecturerRepositoryImpl;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.boundary.impl.StudentServiceImpl;
import kz.zhanbolat.university.student.control.impl.StudentRepositoryImpl;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.boundary.impl.UserServiceImpl;
import kz.zhanbolat.university.user.control.impl.UserRepositoryImpl;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import kz.zhanbolat.university.user.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(classes = {TestDBConfiguration.class,
                              StudentRepositoryImpl.class,
                              UserRepositoryImpl.class,
                              StudentServiceImpl.class,
                              EncryptionConfig.class,
                              MessageEncryptor.class,
                              UserServiceImpl.class,
                              LecturerServiceImpl.class, LecturerRepositoryImpl.class})
@DirtiesContext
public class StudentServiceTest {
    @Autowired
    private StudentService studentService;
    @Autowired
    private UserService userService;

    @Test
    public void check_getStudentByUsername_ReturnStudent() {
        Student student = studentService.getStudentByUsername("user");

        assertEquals(1, student.getId());
        assertEquals("address", student.getAddress());
        assertEquals("email", student.getEmail());
        assertEquals("student fio", student.getFio());
        assertEquals("telephone", student.getPhone());
        assertEquals(LocalDate.of(2020, 3, 20), student.getBirthday());
    }

    @Test
    public void registerNewStudentShouldCreateStudentInDb() {
        User user = User.builder().username("student_username").password("student_password").build();
        userService.registerNewUser(user);
        Student student = Student.builder().user(user).build();

        studentService.registerNewStudent(student);
        Student createdStudent = studentService.getStudentByUsername(user.getUsername());

        assertNotNull(createdStudent);
    }
}
