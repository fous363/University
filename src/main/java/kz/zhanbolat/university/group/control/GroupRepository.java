package kz.zhanbolat.university.group.control;

import kz.zhanbolat.university.group.entity.Group;

import java.util.List;

public interface GroupRepository {
    List<Group> findAll();
    Group findOne(Integer groupId);
    void create(Group group);
    boolean isExisted(Integer id);
    void update(Group group);
    void delete(Integer groupId);
    Group findOneByCode(String groupCode);
    List<Group> findAllByLecturerId(Integer lecturerId);
    boolean checkExists(Group group);
}
