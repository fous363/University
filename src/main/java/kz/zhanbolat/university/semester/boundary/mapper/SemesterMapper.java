package kz.zhanbolat.university.semester.boundary.mapper;

import kz.zhanbolat.university.semester.boundary.dto.SemesterForm;
import kz.zhanbolat.university.semester.entity.Semester;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SemesterMapper {

    public static SemesterForm map(Semester semester) {
        return SemesterForm.builder()
                .id(semester.getId())
                .number(semester.getNumber())
                .name(semester.getName())
                .year(semester.getYear())
                .startDate(semester.getStartDate().format(DateTimeFormatter.ISO_DATE))
                .endDate(semester.getEndDate().format(DateTimeFormatter.ISO_DATE))
                .build();
    }

    public static Semester map(SemesterForm source) {
        return Semester.builder()
                .id(source.getId())
                .number(source.getNumber())
                .name(source.getName())
                .year(source.getYear())
                .startDate(LocalDate.parse(source.getStartDate()))
                .endDate(LocalDate.parse(source.getEndDate()))
                .build();
    }
}
