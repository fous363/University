package kz.zhanbolat.university.location.control;

import kz.zhanbolat.university.location.entity.Location;

import java.util.List;

public interface LocationRepository {
    List<Location> findAll();
    Location findOne(Integer locationId);
    void create(Location location);
    void update(Location location);
    void delete(Integer locationId);
    Location findOneByAudienceAndCampus(String audience, String campus);
    boolean checkExists(Location location);
}
