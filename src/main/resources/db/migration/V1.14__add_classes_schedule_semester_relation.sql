create table classes_schedule_semester(
    classes_schedule_id int not null,
    semester_id int not null,
    foreign key (classes_schedule_id) references classes_schedule(id),
    foreign key (semester_id) references semester(id)
);