CREATE TABLE authority(
    name varchar(10) primary key
);

CREATE TABLE user_account(
    id serial primary key,
    username varchar(60) not null unique,
    credential varchar(100) not null,
    is_active boolean default false,
    authority_name varchar(10),
    foreign key (authority_name) references authority(name)
);

CREATE TABLE log_entry(
    id serial primary key,
    log_status boolean not null,
    date_and_time timestamp not null,
    user_id int not null,
    foreign key (user_id) references user_account(id)
);

CREATE TABLE student(
    id serial primary key,
    fio varchar(120) not null,
    birthday date,
    phone char(16),
    address varchar(80),
    email varchar(80),
    user_id int not null,
    foreign key (user_id) references user_account(id)
);

CREATE TABLE semester(
    id serial primary key,
    number int not null,
    name varchar(70) not null,
    year int not null,
    start_date date not null,
    end_date date
);

CREATE TABLE student_semester(
    student_id int not null,
    semester_id int not null,
    foreign key (student_id) references student(id),
    foreign key (semester_id) references semester(id)
);
