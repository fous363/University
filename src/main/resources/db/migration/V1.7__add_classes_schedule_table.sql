CREATE TABLE location(
    id serial primary key,
    audience varchar(100) not null,
    campus varchar(100) not null
);

CREATE TABLE classes_schedule(
    id serial primary key,
    date_and_time timestamp not null,
    location_id int not null,
    course_code char(10) not null,
    foreign key (location_id) references location(id),
    foreign key (course_code) references course(code)
);

CREATE TABLE student_classes_schedule(
    student_id int not null,
    classes_schedule_id int not null,
    foreign key (student_id) references student(id),
    foreign key (classes_schedule_id) references classes_schedule(id)
);

CREATE TABLE lecturer_classes_schedule(
    lecturer_id int not null,
    classes_schedule_id int not null,
    foreign key (lecturer_id) references lecturer(id),
    foreign key (classes_schedule_id) references classes_schedule(id)
);