CREATE TABLE authority(
    name varchar(10) primary key
);

CREATE TABLE user_account(
    id int primary key auto_increment,
    username varchar(60),
    credential varchar(100),
    is_active boolean,
    authority_name varchar(10),
    foreign key (authority_name) references authority(name)
);

CREATE TABLE log_entry(
    id int primary key auto_increment,
    logged_in boolean,
    logged_off boolean,
    date_and_time timestamp,
    user_id int,
    foreign key (user_id) references user_account(id)
);

create table student_group(
    id int primary key auto_increment,
    code char(10),
    name char(80)
);

CREATE TABLE student(
    id int primary key auto_increment,
    fio varchar(120),
    birthday date,
    phone char(16),
    address varchar(80),
    email varchar(80),
    user_id int,
    group_id int,
    foreign key (user_id) references user_account(id),
    foreign key (group_id) references student_group(id)
);

CREATE TABLE semester(
    id int primary key auto_increment,
    number int,
    name varchar(70),
    year int,
    start_date date,
    end_date date
);

CREATE TABLE student_semester(
    student_id int,
    semester_id int,
    foreign key (student_id) references student(id),
    foreign key (semester_id) references semester(id)
);

CREATE TABLE academic_degree(
    name char(40) primary key not null,
    priority smallint
);

CREATE TABLE lecturer(
    id int primary key auto_increment,
    fio varchar(120) not null,
    birthday date,
    phone char(16),
    address varchar(80),
    email varchar(80),
    user_id int not null,
    academic_degree_name char(40) not null,
    foreign key (user_id) references user_account(id),
    foreign key (academic_degree_name) references academic_degree(name)
);

CREATE TABLE course(
    id int primary key auto_increment,
    code char(10),
    name varchar(100),
    description text
);

CREATE TABLE student_course(
    student_id int,
    course_id int,
    foreign key (student_id) references student(id),
    foreign key (course_id) references course(id)
);

CREATE TABLE lecturer_course(
    lecturer_id int,
    course_id int,
    foreign key (lecturer_id) references lecturer(id),
    foreign key (course_id) references course(id)
);
CREATE TABLE location(
    id int primary key auto_increment,
    audience varchar(100),
    campus varchar(100)
);

CREATE TABLE classes_schedule(
    id int primary key auto_increment,
    date_and_time timestamp,
    location_id int,
    course_id int,
    foreign key (location_id) references location(id),
    foreign key (course_id) references course(id)
);

CREATE TABLE user_classes_schedule(
    user_id int,
    classes_schedule_id int,
    foreign key (user_id) references user_account(id),
    foreign key (classes_schedule_id) references classes_schedule(id)
);

INSERT INTO academic_degree(name, priority) VALUES ('BACHELAR', 1);
INSERT INTO academic_degree(name, priority) VALUES ('MASTER', 2);
INSERT INTO academic_degree(name, priority) VALUES ('Ph.D', 3);

INSERT INTO authority(name) VALUES ('USER_ROLE');

INSERT INTO user_account(username, credential, authority_name) values ('student_user', '0', 'USER_ROLE');
INSERT INTO user_account(username, credential) values ('empty_student_user', '0');
insert into user_account(username, credential, authority_name) values ('lecturer', '0', 'USER_ROLE');

insert into student(fio, birthday, phone, address, email, user_id) values ('student fio', '2020-03-20', 'telephone', 'address', 'email', 1);
insert into student(fio, user_id) values ('empty student', 2);

INSERT INTO semester(number, name, year, start_date, end_date) values (1, 'semester 1', 2020, '2020-03-20', '2021-03-20');

insert into student_semester(student_id, semester_id) values (1, 1);
insert into student_semester(student_id, semester_id) values (2, 1);

INSERT INTO course(code, name) values ('M1', 'Math 1');
INSERT INTO course(code, name) values ('M2', 'Math 2');

INSERT INTO location (audience, campus) values ('508', 'FIT');

insert into classes_schedule(date_and_time, location_id, course_id) values ('2020-04-01 20:14:00', 1, 1);

insert into user_classes_schedule(user_id, classes_schedule_id) values (1, 1);

insert into lecturer(fio, academic_degree_name, user_id) values ('lecturer', 'BACHELAR', 3);

insert into student_course(student_id, course_id) values (1, 1);
insert into student_course(student_id, course_id) values (1, 2);
insert into student_course(student_id, course_id) values (2, 1);

insert into lecturer_course(lecturer_id, course_id) values (1, 1);