create table student_group(
    id serial primary key,
    code char(10) not null,
    name char(80) not null
);

alter table student
add column group_id int null;

alter table student
add foreign key (group_id) references student_group(id);