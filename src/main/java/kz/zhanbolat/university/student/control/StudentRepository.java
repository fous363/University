package kz.zhanbolat.university.student.control;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.student.entity.Student;

import java.util.List;

public interface StudentRepository {
    Student findOneByUsername(String username);
    void create(Student student);
    void updateStudentInfoByUsername(Student student, String username);
    List<Student> findAll();
    Student findOne(Integer studentId);
    void updateStudentGroup(Integer id, Integer groupId);
    List<Student> findAllByCourses(List<Course> courses);
    List<Student> findAllByGroup(Integer groupId);
    List<Student> findAllBySchedule(Integer scheduleId);
}
