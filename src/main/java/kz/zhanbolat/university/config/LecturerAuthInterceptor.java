package kz.zhanbolat.university.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class LecturerAuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String token = (String) request.getSession().getAttribute("access-token");
        String authorityName = UserAuthorityTable.INSTANCE.getAuthorityName(token);
        return Objects.equals(authorityName, "USER_LECTURER");
    }
}
