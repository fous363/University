package kz.zhanbolat.university.group.boundary;

import kz.zhanbolat.university.group.entity.Group;

import java.util.List;

public interface GroupService {
    List<Group> getAllGroups();
    Group getGroup(Integer groupId);
    void createGroup(Group group);
    void updateGroup(Group group);
    void deleteGroup(Integer groupId);
    Group getGroup(String groupCode);
    List<Group> getAllGroups(Integer lecturerId);
}
