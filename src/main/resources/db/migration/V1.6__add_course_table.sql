CREATE TABLE course(
    code char(10) primary key not null,
    name varchar(100) not null,
    description text
);

CREATE TABLE student_course(
    student_id int not null,
    course_code char(10) not null,
    foreign key (student_id) references student(id),
    foreign key (course_code) references course(code)
);

CREATE TABLE lecturer_course(
    lecturer_id int not null,
    course_code char(10) not null,
    foreign key (lecturer_id) references lecturer(id),
    foreign key (course_code) references course(code)
);