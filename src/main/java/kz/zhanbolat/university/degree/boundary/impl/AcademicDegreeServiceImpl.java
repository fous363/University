package kz.zhanbolat.university.degree.boundary.impl;

import kz.zhanbolat.university.degree.boundary.AcademicDegreeService;
import kz.zhanbolat.university.degree.control.AcademicDegreeRepository;
import kz.zhanbolat.university.degree.entity.AcademicDegree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AcademicDegreeServiceImpl implements AcademicDegreeService {
    @Autowired
    private AcademicDegreeRepository academicDegreeRepository;

    @Override
    public List<AcademicDegree> getAllAcademicDegrees() {
        return academicDegreeRepository.findAll();
    }

    @Override
    public AcademicDegree getAcademicDegree(String name) {
        return academicDegreeRepository.findOne(name);
    }
}
