package kz.zhanbolat.university.course.control;

import kz.zhanbolat.university.course.entity.Course;

import java.util.List;

public interface CourseRepository {
    List<Course> findAllByUsername(String username);
    void create(Course course);
    boolean isCourseExisted(String code);
    void delete(String courseCode);
    List<Course> findAll();
    Course findOne(String courseCode);
    void update(Course course);
}
