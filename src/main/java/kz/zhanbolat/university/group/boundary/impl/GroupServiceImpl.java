package kz.zhanbolat.university.group.boundary.impl;

import kz.zhanbolat.university.group.boundary.GroupService;
import kz.zhanbolat.university.group.control.GroupRepository;
import kz.zhanbolat.university.group.entity.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {
    @Autowired
    private GroupRepository groupRepository;

    @Override
    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    @Override
    public Group getGroup(Integer groupId) {
        return groupRepository.findOne(groupId);
    }

    @Override
    public void createGroup(Group group) {
        if (groupRepository.checkExists(group)) {
            throw new IllegalArgumentException("Group already exists");
        }
        groupRepository.create(group);
    }

    @Override
    public void updateGroup(Group group) {
        if (groupRepository.checkExists(group)) {
            throw new IllegalArgumentException("Group already exists");
        }
        groupRepository.update(group);
    }

    @Override
    public void deleteGroup(Integer groupId) {
        groupRepository.delete(groupId);
    }

    @Override
    public Group getGroup(String groupCode) {
        return groupRepository.findOneByCode(groupCode);
    }

    @Override
    public List<Group> getAllGroups(Integer lecturerId) {
        return groupRepository.findAllByLecturerId(lecturerId);
    }
}
