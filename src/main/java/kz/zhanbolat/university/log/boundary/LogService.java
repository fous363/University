package kz.zhanbolat.university.log.boundary;

import kz.zhanbolat.university.log.entity.Log;

import java.util.List;

public interface LogService {
    void createLogOfLogIn(String login);
    void createLogOfLogOut(String login);
    List<Log> getAllLogs();
}
