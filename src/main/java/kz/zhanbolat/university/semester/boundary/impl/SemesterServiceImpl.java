package kz.zhanbolat.university.semester.boundary.impl;

import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.semester.control.SemesterRepository;
import kz.zhanbolat.university.semester.entity.Semester;
import kz.zhanbolat.university.utils.DateValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
//TODO: add Tests cases
public class SemesterServiceImpl implements SemesterService {
    @Autowired
    private SemesterRepository semesterRepository;

    @Override
    public List<Semester> getAllStudentSemesters(Integer studentId) {
        if (studentId < 1) {
            throw new IllegalArgumentException("id should not be below 1");
        }
        return semesterRepository.findAllByStudentId(studentId);
    }

    @Override
    //TODO: add better validation
    public void createSemester(Semester semester) {
        if (!DateValidator.validateSemesterDates(semester)) {
            throw new IllegalArgumentException("Invalid dates");
        }
        semesterRepository.create(semester);
    }

    @Override
    public void deleteSemester(Integer semesterId) {
        semesterRepository.delete(semesterId);
    }

    @Override
    public List<Semester> getAllSemesters() {
        return semesterRepository.findAll();
    }

    @Override
    public void updateSemester(Semester semester) {
        if (!DateValidator.validateSemesterDates(semester)) {
            throw new IllegalArgumentException("Invalid dates");
        }
        semesterRepository.update(semester);
    }

    @Override
    public Semester getSemester(Integer semesterId) {
        return semesterRepository.findOne(semesterId);
    }
}
