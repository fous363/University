package kz.zhanbolat.university.location.boundary;

import kz.zhanbolat.university.location.boundary.dto.LocationForm;
import kz.zhanbolat.university.location.boundary.mapper.LocationMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/location")
public class LocationController {
    private static final Logger logger = LogManager.getLogger(LocationController.class);
    @Autowired
    private LocationService locationService;

    @GetMapping("/admin")
    public String adminLocationsPage(ModelMap model) {
        model.addAttribute("locations",
                locationService.getAllLocations().stream().map(LocationMapper::map).collect(Collectors.toList()));
        return "adminLocations";
    }

    @GetMapping("/admin/create")
    public String adminLocationCreatePage(ModelMap model) {
        model.addAttribute("location", new LocationForm());
        return "adminLocationCreate";
    }

    @GetMapping("/admin/update/{location_id}")
    public String adminLocationUpdatePage(@PathVariable("location_id") Integer locationId, ModelMap model) {
        model.addAttribute("location", LocationMapper.map(locationService.getLocation(locationId)));
        return "adminLocationUpdate";
    }

    @PostMapping("/admin/create")
    public String processAdminLocationCreate(@ModelAttribute("location")LocationForm location, ModelMap model) {
        try {
            locationService.createLocation(LocationMapper.map(location));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminLocationCreatePage(model);
        }
        return "redirect:/location/admin";
    }

    @PostMapping("/admin/update")
    public String processAdminLocationUpdate(@ModelAttribute("location") LocationForm location, ModelMap model) {
        try {
            locationService.updateLocation(LocationMapper.map(location));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminLocationUpdatePage(location.getId(), model);
        }
        return "redirect:/location/admin";
    }

    @GetMapping("/admin/delete/{location_id}")
    public String processAdminLocationDelete(@PathVariable("location_id")Integer locationId) {
        locationService.deleteLocation(locationId);
        return "redirect:/location/admin";
    }
}
