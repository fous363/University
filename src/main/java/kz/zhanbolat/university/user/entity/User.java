package kz.zhanbolat.university.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.log.entity.Log;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.entity.Student;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_account")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    @Column(name = "credential")
    private String password;
    @Column(name = "is_active")
    private Boolean isActive;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Log> logList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "authority_name", referencedColumnName = "name")
    private Authority authority;

    @OneToOne(mappedBy = "user")
    private Student student;
    @OneToOne(mappedBy = "user")
    private Lecturer lecturer;

    @ManyToMany
    @JoinTable(name = "user_classes_schedule", joinColumns = @JoinColumn(name = "user_id"),
                inverseJoinColumns = @JoinColumn(name = "classes_schedule_id"))
    private List<ClassesSchedule> schedules;

    @ManyToMany(mappedBy = "users", targetEntity = Course.class)
    private List<Course> courses;

    public User() {
    }

    public User(Builder builder) {
        id = builder.id;
        username = builder.username;
        password = builder.password;
        isActive = builder.isActive;
        authority = builder.authority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<Log> getLogList() {
        return logList;
    }

    public void setLogList(List<Log> logList) {
        this.logList = logList;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("username", username)
                .append("password", password)
                .append("isActive", isActive)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return new EqualsBuilder()
                .append(id, user.id)
                .append(username, user.username)
                .append(password, user.password)
                .append(isActive, user.isActive)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(username)
                .append(password)
                .append(isActive)
                .toHashCode();
    }

    public static class Builder {
        private Integer id;
        private String username;
        private String password;
        private Boolean isActive;
        private Authority authority;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;

            return this;
        }

        public Builder isActive(Boolean isActive) {
            this.isActive = isActive;

            return this;
        }

        public Builder authority(Authority authority) {
            this.authority = authority;

            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
