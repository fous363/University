package kz.zhanbolat.university.location.control.impl;

import kz.zhanbolat.university.location.control.LocationRepository;
import kz.zhanbolat.university.location.entity.Location;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class LocationRepositoryImpl implements LocationRepository {
    private static final String SELECT_ALL = "select id, audience, campus from location";
    private static final String DELETE = "delete from location where id=:location_id";
    private static final String SELECT_ONE_BY_AUDIENCE_AND_CAMPUS = "select id, audience, campus " +
            "from location where audience = :audience and campus = :campus";
    private static final String SELECT_COUNT_BY_AUDIENCE_AND_CAMPUS = "select count(*) from location " +
            "where audience=:audience and campus=:campus";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Location> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, Location.class);
        return query.getResultList();
    }

    @Override
    public Location findOne(Integer locationId) {
        return entityManager.find(Location.class, locationId);
    }

    @Override
    public void create(Location location) {
        entityManager.persist(location);
    }

    @Override
    public void update(Location location) {
        entityManager.merge(location);
    }

    @Override
    public void delete(Integer locationId) {
        Query query = entityManager.createNativeQuery(DELETE);
        query.setParameter("location_id", locationId);
        query.executeUpdate();
    }

    @Override
    public Location findOneByAudienceAndCampus(String audience, String campus) {
        Query query = entityManager.createNativeQuery(SELECT_ONE_BY_AUDIENCE_AND_CAMPUS, Location.class);
        query.setParameter("audience", audience);
        query.setParameter("campus", campus);
        return (Location) query.getSingleResult();
    }

    @Override
    public boolean checkExists(Location location) {
        Query query = entityManager.createNativeQuery(SELECT_COUNT_BY_AUDIENCE_AND_CAMPUS);
        query.setParameter("audience", location.getAudience());
        query.setParameter("campus", location.getCampus());
        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }
}
