package kz.zhanbolat.university.semester.control;

import kz.zhanbolat.university.semester.entity.Semester;

import java.util.List;

public interface SemesterRepository {
    List<Semester> findAllByStudentId(Integer studentId);
    void create(Semester semester);
    void delete(Integer semesterId);
    List<Semester> findAll();
    void update(Semester semester);
    Semester findOne(Integer semesterId);
}
