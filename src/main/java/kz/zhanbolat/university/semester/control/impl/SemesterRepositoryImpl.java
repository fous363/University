package kz.zhanbolat.university.semester.control.impl;

import kz.zhanbolat.university.semester.control.SemesterRepository;
import kz.zhanbolat.university.semester.entity.Semester;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class SemesterRepositoryImpl implements SemesterRepository {
    private static final String SELECT_ALL_BY_STUDENT_ID = "select id, number, name, year, start_date, end_date " +
            "from semester inner join student_semester on student_semester.semester_id = semester.id" +
            " where student_semester.student_id = :student_id";
    private static final String DELETE_SEMESTER = "delete from semester where id=:semester_id";
    private static final String SELECT_ALL = "select id, number, name, year, start_date, end_date from semester";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Semester> findAllByStudentId(Integer studentId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_STUDENT_ID, Semester.class);
        query.setParameter("student_id", studentId);
        return query.getResultList();
    }

    @Override
    public void create(Semester semester) {
        entityManager.persist(semester);
    }

    @Override
    public void delete(Integer semesterId) {
        Query query = entityManager.createNativeQuery(DELETE_SEMESTER);
        query.setParameter("semester_id", semesterId);
        query.executeUpdate();
    }

    @Override
    public List<Semester> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, Semester.class);
        return query.getResultList();
    }

    @Override
    public void update(Semester semester) {
        entityManager.merge(semester);
    }

    @Override
    public Semester findOne(Integer semesterId) {
        return entityManager.find(Semester.class, semesterId);
    }
}
