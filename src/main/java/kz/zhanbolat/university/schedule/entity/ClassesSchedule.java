package kz.zhanbolat.university.schedule.entity;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.location.entity.Location;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.entity.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "classes_schedule")
public class ClassesSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "date_and_time")
    private LocalDateTime dateTime;

    @ManyToOne(targetEntity = Location.class)
    @JoinColumn(name = "location_id")
    private Location location;

    @ManyToOne(targetEntity = Course.class)
    @JoinColumn(name = "course_id")
    private Course course;
    @ManyToMany(mappedBy = "schedules")
    private List<User> users;

    public ClassesSchedule() {
    }

    public ClassesSchedule(Integer id, LocalDateTime dateTime) {
        this.id = id;
        this.dateTime = dateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("dateTime", dateTime)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ClassesSchedule that = (ClassesSchedule) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(dateTime, that.dateTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(dateTime)
                .toHashCode();
    }
}
