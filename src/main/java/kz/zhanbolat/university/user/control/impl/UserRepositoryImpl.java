package kz.zhanbolat.university.user.control.impl;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.user.control.UserRepository;
import kz.zhanbolat.university.user.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final String COUNT_USER_WITH_USERNAME_AND_PASS = "select count(id) from user_account " +
            "where username=:username and credential=:credential";
    private static final String SELECT_USER_BY_USERNAME = "select id, username, credential, is_active, " +
            "authority_name from user_account where username=:username";
    private static final String SELECT_AUTHORITY_NAME_BY_USERNAME = "select authority.name from user_account " +
            "inner join authority on user_account.authority_name = authority.name " +
            "where user_account.username = :username";
    private static final String UPDATE_CREDENTIAL = "update user_account set credential=:credential " +
            "where username=:username";
    private static final String UPDATE_AUTHORITY = "update user_account set authority_name=:authority where username=:username";
    private static final String SELECT_ALL_EXCLUDE_ONE = "select id, username, credential, is_active, authority_name " +
            "from user_account where id!=:user_id";
    private static final String DELETE_BY_USERNAME = "delete from user_account where username=:username";
    private static final String UPDATE_IS_ACTIVE_BY_USERNAME = "update user_account set is_active=:active_status " +
            "where username=:username";
    private static final String SELECT_ONE_BY_STUDENT_ID = "select user_account.id, user_account.username, " +
            "user_account.credential, user_account.is_active, user_account.authority_name from user_account " +
            "inner join student on student.user_id = user_account.id where student.id = :student_id";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findOneByUsername(String username) {
        Query query = entityManager.createNativeQuery(SELECT_USER_BY_USERNAME, User.class);
        query.setParameter("username", username);
        return (User) query.getSingleResult();
    }

    @Override
    public boolean checkUserExists(String username, String password) {
        Query query = entityManager.createNativeQuery(COUNT_USER_WITH_USERNAME_AND_PASS);
        query.setParameter("username", username);
        query.setParameter("credential", password);
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue() == 1;
    }

    @Override
    public void create(User user) {
        entityManager.persist(user);
    }

    @Override
    public String findAuthorityByUsername(String username) {
        Query query = entityManager.createNativeQuery(SELECT_AUTHORITY_NAME_BY_USERNAME);
        query.setParameter("username", username);
        return (String) query.getSingleResult();
    }

    @Override
    public void updatePassword(String username, String password) {
        Query query = entityManager.createNativeQuery(UPDATE_CREDENTIAL);
        query.setParameter("credential", password);
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public void updateUserAuthority(String username, String authority) {
        Query query = entityManager.createNativeQuery(UPDATE_AUTHORITY);
        query.setParameter("authority", authority);
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public List<User> findAllWithExcludeOne(User user) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_EXCLUDE_ONE, User.class);
        query.setParameter("user_id", user.getId());
        return query.getResultList();
    }

    @Override
    public void deleteByUsername(String username) {
        Query query = entityManager.createNativeQuery(DELETE_BY_USERNAME);
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public void updateUserActiveStatus(String username, boolean activeStatus) {
        Query query = entityManager.createNativeQuery(UPDATE_IS_ACTIVE_BY_USERNAME);
        query.setParameter("active_status", activeStatus);
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public void findAllStudentUsersByCoursesAndLecturer(List<Course> courses, String username) {

    }

    @Override
    public User findOneByStudentId(Integer studentId) {
        Query query = entityManager.createNativeQuery(SELECT_ONE_BY_STUDENT_ID, User.class);
        query.setParameter("student_id", studentId);
        return (User) query.getSingleResult();
    }
}
