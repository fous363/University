package kz.zhanbolat.university.student.control.impl;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.student.control.StudentRepository;
import kz.zhanbolat.university.student.entity.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Date;
import java.util.List;

@Repository
public class StudentRepositoryImpl implements StudentRepository {
    private static final String SELECT_STUDENT_BY_USERNAME = "select student.id, student.fio, student.birthday, " +
            "student.phone, student.address, student.email, student.user_id, student.group_id from student " +
            "inner join user_account on user_account.id = student.user_id " +
            "where user_account.username=:username";
    private static final String UPDATE_STUDENT_INFO_BY_USERNAME = "UPDATE student " +
            "SET fio=:fio, birthday=:birthday, phone=:phone, email=:email, address=:address " +
            "FROM user_account WHERE user_account.username=:username";
    private static final String SELECT_ALL = "select id, fio, birthday, address, email, phone, user_id, group_id from student";
    private static final String UPDATE_STUDENT_GROUP = "update student set group_id=:group_id where id=:student_id";
    private static final String SELECT_ALL_BY_COURSES = "select student.id, student.fio, student.birthday, " +
            "student.phone, student.address, student.email, student.user_id, student.group_id from student " +
            "inner join user_course on user_course.course_id in (:courses)" +
            "inner join user_account on user_account.id = user_course.user_id and student.user_id = user_account.id";
    private static final String SELECT_ALL_BY_GROUP_ID = "select id, fio, birthday, phone, address, email, user_id," +
            " group_id from student where group_id = :group_id";
    private static final String SELECT_ALL_BY_SCHEDULE_ID = "select distinct student.id, student.fio, student.birthday, " +
            "student.phone, student.address, student.email, student.user_id, student.group_id from student " +
            "inner join user_classes_schedule on user_classes_schedule.classes_schedule_id = :schedule_id " +
            "and user_classes_schedule.user_id = student.user_id";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Student findOneByUsername(String username) {
        Query query = entityManager.createNativeQuery(SELECT_STUDENT_BY_USERNAME, Student.class);
        query.setParameter("username", username);
        return (Student) query.getSingleResult();
    }

    @Override
    public void create(Student student) {
        entityManager.persist(student);
    }

    @Override
    public void updateStudentInfoByUsername(Student student, String username) {
        Query query = entityManager.createNativeQuery(UPDATE_STUDENT_INFO_BY_USERNAME);
        query.setParameter("fio", student.getFio());
        query.setParameter("birthday", Date.valueOf(student.getBirthday()));
        query.setParameter("address", student.getAddress());
        query.setParameter("email", student.getEmail());
        query.setParameter("phone", student.getPhone());
        query.setParameter("username", username);
        query.executeUpdate();
    }

    @Override
    public List<Student> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, Student.class);
        return query.getResultList();
    }

    @Override
    public Student findOne(Integer studentId) {
        return entityManager.find(Student.class, studentId);
    }

    @Override
    public void updateStudentGroup(Integer id, Integer groupId) {
        Query query = entityManager.createNativeQuery(UPDATE_STUDENT_GROUP);
        query.setParameter("group_id", groupId);
        query.setParameter("student_id", id);
        query.executeUpdate();
    }

    @Override
    public List<Student> findAllByCourses(List<Course> courses) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_COURSES, Student.class);
        query.setParameter("courses", courses);
        return query.getResultList();
    }

    @Override
    public List<Student> findAllByGroup(Integer groupId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_GROUP_ID, Student.class);
        query.setParameter("group_id", groupId);
        return query.getResultList();
    }

    @Override
    public List<Student> findAllBySchedule(Integer scheduleId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_SCHEDULE_ID, Student.class);
        query.setParameter("schedule_id", scheduleId);
        return query.getResultList();
    }
}
