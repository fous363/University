package kz.zhanbolat.university.log.boundary.impl;

import kz.zhanbolat.university.log.control.LogRepository;
import kz.zhanbolat.university.log.boundary.LogService;
import kz.zhanbolat.university.log.control.impl.LogRepositoryImpl;
import kz.zhanbolat.university.log.entity.Log;
import kz.zhanbolat.university.log.entity.LogStatus;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.boundary.impl.UserServiceImpl;
import kz.zhanbolat.university.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional(rollbackOn = RuntimeException.class)
@ComponentScan(basePackageClasses = {LogRepositoryImpl.class, UserServiceImpl.class})
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private UserService userService;

    @Override
    public void createLogOfLogIn(String login) {
        User user = userService.findUserByUsername(login);
        LogStatus logStatus = LogStatus.LOG_IN;
        Log log = Log.builder()
                        .timeStamp(LocalDateTime.now())
                        .loggedIn(logStatus.isLoggedIn())
                        .loggedOff(logStatus.isLoggedOff())
                        .user(user)
                        .build();
        logRepository.create(log);
    }

    @Override
    public void createLogOfLogOut(String login) {
        User user = userService.findUserByUsername(login);
        LogStatus logStatus = LogStatus.LOG_OUT;
        Log log = Log.builder()
                        .timeStamp(LocalDateTime.now())
                        .loggedIn(logStatus.isLoggedIn())
                        .loggedOff(logStatus.isLoggedOff())
                        .user(user)
                        .build();
        logRepository.create(log);
    }

    @Override
    public List<Log> getAllLogs() {
        return logRepository.findAll();
    }
}
