package kz.zhanbolat.university.lecturer.boundary.mapper;

import kz.zhanbolat.university.lecturer.boundary.dto.LecturerForm;
import kz.zhanbolat.university.lecturer.entity.Lecturer;

public final class LecturerMapper {

    public static Lecturer map(LecturerForm source) {
        return Lecturer.builder()
                .fio(source.getFio())
                .address(source.getAddress())
                .birthday(source.getBirthday())
                .email(source.getEmail())
                .phone(source.getPhone())
                .build();
    }

    public static LecturerForm map(Lecturer source) {
        return LecturerForm.builder()
                .fio(source.getFio())
                .address(source.getAddress())
                .birthday(source.getBirthday())
                .email(source.getEmail())
                .phone(source.getPhone())
                .academicDegreeName(source.getAcademicDegree().getName())
                .build();
    }
}
