package kz.zhanbolat.university.lecturer.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class LecturerForm {
    private String fio;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;
    private String phone;
    private String address;
    private String email;
    private String academicDegreeName;

    public LecturerForm() {
    }

    private LecturerForm(Builder builder) {
        this.fio = builder.fio;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.address = builder.address;
        this.email = builder.email;
        this.academicDegreeName = builder.academicDegreeName;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAcademicDegreeName() {
        return academicDegreeName;
    }

    public void setAcademicDegreeName(String academicDegreeName) {
        this.academicDegreeName = academicDegreeName;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("fio", fio)
                .append("birthday", birthday)
                .append("phone", phone)
                .append("address", address)
                .append("email", email)
                .append("academicDegreeName", academicDegreeName)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LecturerForm that = (LecturerForm) o;

        return new EqualsBuilder()
                .append(fio, that.fio)
                .append(birthday, that.birthday)
                .append(phone, that.phone)
                .append(address, that.address)
                .append(email, that.email)
                .append(academicDegreeName, that.academicDegreeName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(fio)
                .append(birthday)
                .append(phone)
                .append(address)
                .append(email)
                .append(academicDegreeName)
                .toHashCode();
    }

    public static class Builder {
        private String fio;
        private LocalDate birthday;
        private String phone;
        private String address;
        private String email;
        private String academicDegreeName;

        private Builder() {
        }

        public Builder fio(String fio) {
            this.fio = fio;

            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;

            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;

            return this;
        }

        public Builder address(String address) {
            this.address = address;

            return this;
        }

        public Builder email(String email) {
            this.email = email;

            return this;
        }

        public Builder academicDegreeName(String academicDegreeName) {
            this.academicDegreeName = academicDegreeName;

            return this;
        }

        public LecturerForm build() {
            return new LecturerForm(this);
        }
    }
}
