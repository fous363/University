package kz.zhanbolat.university;

import kz.zhanbolat.university.config.EncryptionConfig;
import kz.zhanbolat.university.config.TestDBConfiguration;
import kz.zhanbolat.university.schedule.boundary.ClassesScheduleService;
import kz.zhanbolat.university.schedule.boundary.impl.ClassesScheduleServiceImpl;
import kz.zhanbolat.university.schedule.control.impl.ClassesScheduleRepositoryImpl;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.boundary.impl.StudentServiceImpl;
import kz.zhanbolat.university.student.control.impl.StudentRepositoryImpl;
import kz.zhanbolat.university.user.boundary.impl.UserServiceImpl;
import kz.zhanbolat.university.user.control.impl.UserRepositoryImpl;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(classes = {TestDBConfiguration.class,
        ClassesScheduleServiceImpl.class, ClassesScheduleRepositoryImpl.class,
        StudentServiceImpl.class, StudentRepositoryImpl.class, UserServiceImpl.class,
        UserRepositoryImpl.class, MessageEncryptor.class, EncryptionConfig.class})
@DirtiesContext
public class ClassesScheduleServiceTest {
    @Autowired
    private ClassesScheduleService classesScheduleService;

    @Test
    @Disabled
    public void check_getStudentClassesSchedule_ReturnClassesScheduleList() {
        String username = "student_user";
    }

    @Test
    @Disabled
    public void check_getStudentClassesSchedule_ReturnEmptyClassesScheduleList() {
        String username = "empty_student_user";
    }
}
