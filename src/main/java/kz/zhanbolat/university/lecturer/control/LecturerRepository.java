package kz.zhanbolat.university.lecturer.control;

import kz.zhanbolat.university.lecturer.entity.Lecturer;

public interface LecturerRepository {
    void create(Lecturer lecturer);
    Lecturer findOneByUsername(String username);
    void updateLecturerInfoByUsername(Lecturer lecturer, String username);
}
