package kz.zhanbolat.university.location.entity;

import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String audience;
    private String campus;

    @OneToMany(mappedBy = "location", targetEntity = ClassesSchedule.class)
    private List<ClassesSchedule> scheduleList;

    public Location() {
    }

    public Location(Integer id, String audience, String campus) {
        this.id = id;
        this.audience = audience;
        this.campus = campus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public List<ClassesSchedule> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<ClassesSchedule> scheduleList) {
        this.scheduleList = scheduleList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("audience", audience)
                .append("campus", campus)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        return new EqualsBuilder()
                .append(id, location.id)
                .append(audience, location.audience)
                .append(campus, location.campus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(audience)
                .append(campus)
                .toHashCode();
    }
}
