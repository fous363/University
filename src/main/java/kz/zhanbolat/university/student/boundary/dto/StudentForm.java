package kz.zhanbolat.university.student.boundary.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class StudentForm {
    private Integer id;
    private String fio;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;
    private String phone;
    private String address;
    private String email;
    private String group;

    public StudentForm() {
    }

    private StudentForm(Builder builder) {
        this.id = builder.id;
        this.fio = builder.fio;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.address = builder.address;
        this.email = builder.email;
        this.group = builder.group;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String fio;
        private LocalDate birthday;
        private String phone;
        private String address;
        private String email;
        private String group;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder fio(String fio) {
            this.fio = fio;
            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder group(String group) {
            this.group = group;

            return this;
        }

        public StudentForm build() {
            return new StudentForm(this);
        }
    }
}
