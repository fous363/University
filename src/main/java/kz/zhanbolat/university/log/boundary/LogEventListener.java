package kz.zhanbolat.university.log.boundary;

import kz.zhanbolat.university.log.boundary.impl.LogServiceImpl;
import kz.zhanbolat.university.log.entity.event.LogEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackageClasses = {LogServiceImpl.class})
public class LogEventListener implements ApplicationListener<LogEvent> {
    private static final Logger logger = LogManager.getLogger(LogEventListener.class);
    @Autowired
    private LogService logService;

    @Override
    public void onApplicationEvent(LogEvent logEvent) {
        logger.info("Get a log event of {}, {}", logEvent.getUsername(), logEvent.getLogStatus());
        switch (logEvent.getLogStatus()) {
            case LOG_IN:
                logService.createLogOfLogIn(logEvent.getUsername());
                break;
            case LOG_OUT:
                logService.createLogOfLogOut(logEvent.getUsername());
                break;
            default:
                throw new IllegalStateException();
        }
    }
}
