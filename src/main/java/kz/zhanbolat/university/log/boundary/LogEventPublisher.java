package kz.zhanbolat.university.log.boundary;

import kz.zhanbolat.university.log.entity.LogStatus;
import kz.zhanbolat.university.log.entity.event.LogEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackageClasses = {ApplicationEventPublisher.class})
public class LogEventPublisher {
    private static final Logger logger = LogManager.getLogger(LogEventPublisher.class);
    @Autowired
    private ApplicationEventPublisher publisher;

    public void publishLogEvent(String username, LogStatus logStatus) {
        logger.info("Publish log event of {}, {}", username, logStatus);
        publisher.publishEvent(new LogEvent(this, username, logStatus));
    }
}
