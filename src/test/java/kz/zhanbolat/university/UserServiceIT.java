package kz.zhanbolat.university;

import kz.zhanbolat.university.config.EncryptionConfig;
import kz.zhanbolat.university.config.TestDBConfiguration;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.control.impl.UserRepositoryImpl;
import kz.zhanbolat.university.user.boundary.impl.UserServiceImpl;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import kz.zhanbolat.university.user.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(classes = {UserServiceImpl.class,
                              UserRepositoryImpl.class,
                              TestDBConfiguration.class,
                              MessageEncryptor.class,
                              EncryptionConfig.class})
@DirtiesContext
public class UserServiceIT {
    private static final Logger logger = LogManager.getLogger(UserServiceIT.class);
    private static final String LOGIN = "user";
    private static final String PASSWORD= "pass";
    @Autowired
    private UserService userService;
    @Autowired
    private MessageEncryptor encryptor;
    private User user;

    @BeforeEach
    public void setUp() {
        user = User.builder().username(LOGIN).password(PASSWORD).build();
    }

    @Test
    public void registerNewUserShouldThrowExceptionOnExistedUser() {
        User newUser = User.builder().username("user5").password("pass").build();
        userService.registerNewUser(newUser);
        User existedUser = User.builder().username("user5").password("pass").build();
        assertThrows(IllegalArgumentException.class, () -> userService.registerNewUser(existedUser));
    }

    @Test
    public void authenticateUserShouldReturnTrueAfterCreationOfNewUser() {
        String login = "New login";
        String password = "New pass";
        user.setUsername(login);
        user.setPassword(password);

        userService.registerNewUser(user);
        boolean result = userService.authenticateUser(login, password);

        assertTrue(result);
    }

    @Test
    public void findUserByLoginShouldReturnUser() {
        User user = userService.findUserByUsername(LOGIN);

        assertNotNull(user);
        assertEquals(LOGIN, user.getUsername());
        assertEquals(PASSWORD, user.getPassword());
    }

    @Test
    public void findUserByLoginShouldThrowExceptionOnNotExistedUser() {
        assertThrows(Exception.class, () -> userService.findUserByUsername("Not a login"));
    }

    @Test
    public void getUserAuthorityShouldReturnExpectedAuthority() {
        String expectedAuthority = "USER_ROLE";

        String userAuthority = userService.getUserAuthority(LOGIN);

        assertEquals(expectedAuthority, userAuthority);
    }

    @Test
    public void getUserAuthorityShouldThrowExceptionOnNotExistedUser() {
        assertThrows(Exception.class, () -> userService.getUserAuthority("Not a login"));
    }

    @Test
    public void changePasswordShouldReturnExpectedPassword() {
        String expectedPassword = "newPassword";
        User user = User.builder().username("userWithChangePassword").password("password").build();
        userService.registerNewUser(user);
        userService.changePassword(user.getUsername(), expectedPassword);
        User userWithChangePassword = userService.findUserByUsername(user.getUsername());
        assertEquals(userWithChangePassword.getPassword(), encryptor.encryptMessage(expectedPassword));
    }
}
