package kz.zhanbolat.university.user.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserRegistrationForm {
    private String username;
    private String password;
    private String singUpAs;

    public UserRegistrationForm() {
    }

    public UserRegistrationForm(String username, String password, String singUpAs) {
        this.username = username;
        this.password = password;
        this.singUpAs = singUpAs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSingUpAs() {
        return singUpAs;
    }

    public void setSingUpAs(String singUpAs) {
        this.singUpAs = singUpAs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("username", username)
                .append("password", password)
                .append("singUpAs", singUpAs)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UserRegistrationForm that = (UserRegistrationForm) o;

        return new EqualsBuilder()
                .append(username, that.username)
                .append(password, that.password)
                .append(singUpAs, that.singUpAs)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(username)
                .append(password)
                .append(singUpAs)
                .toHashCode();
    }
}
