package kz.zhanbolat.university.log.control.impl;

import kz.zhanbolat.university.log.control.LogRepository;
import kz.zhanbolat.university.log.entity.Log;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class LogRepositoryImpl implements LogRepository {
    private static final String SELECT_ALL = "select log_entry.id, log_entry.logged_in, log_entry.logged_off, " +
            "log_entry.date_and_time, log_entry.user_id from log_entry ";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Log log) {
        entityManager.persist(log);
    }

    @Override
    public List<Log> findAll() {
        final Query query = entityManager.createNativeQuery(SELECT_ALL, Log.class);
        return query.getResultList();
    }
}
