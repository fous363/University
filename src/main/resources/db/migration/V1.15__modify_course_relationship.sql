DROP TABLE student_course;
DROP TABLE lecturer_coures;

CREATE TABLE user_course(
    user_id int not null,
    course_id int not null,
    foreign key (user_id) references user_account(id),
    foreign key (course_id) references course(id)
);