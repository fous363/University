package kz.zhanbolat.university;

import kz.zhanbolat.university.user.control.UserRepository;
import kz.zhanbolat.university.user.boundary.impl.UserServiceImpl;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import kz.zhanbolat.university.user.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    private static final String USERNAME = "user";
    private static final String PASSWORD = "password";
    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private MessageEncryptor messageEncryptor;
    private User user;

    @BeforeEach
    public void setUp() {
        user = User.builder().username(USERNAME).password(PASSWORD).build();
    }

    @Test
    public void authenticateUserShouldReturnTrue() {
        when(userRepository.checkUserExists(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(messageEncryptor.encryptMessage(PASSWORD)).thenReturn(PASSWORD);
        boolean result = userService.authenticateUser(USERNAME, PASSWORD);

        assertTrue(result);
    }

    @Test
    public void authenticateUserShouldReturnFalse() {
        when(userRepository.checkUserExists(anyString(), anyString())).thenReturn(Boolean.FALSE);
        when(messageEncryptor.encryptMessage(PASSWORD)).thenReturn(PASSWORD);
        boolean result = userService.authenticateUser(USERNAME, PASSWORD);

        assertFalse(result);
    }

    @Test
    public void registerNewUserShouldThrowExceptionOnEmptyLogin() {
        user.setUsername("");

        assertThrows(IllegalArgumentException.class, () -> userService.registerNewUser(user));
    }

    @Test
    public void registerNewUserShouldThrowExceptionOnEmptyPassword() {
        user.setPassword("");

        assertThrows(IllegalArgumentException.class, () -> userService.registerNewUser(user));
    }

    @Test
    public void registerNewUserShouldThrowExceptionOnExistedUser() {
        when(messageEncryptor.encryptMessage(anyString())).thenReturn(PASSWORD);
        when(userRepository.checkUserExists(anyString(), anyString())).thenReturn(Boolean.TRUE);

        assertThrows(IllegalArgumentException.class, () -> userService.registerNewUser(user));
    }

    @Test
    public void findUserByLoginShouldReturnUser() {
        when(userRepository.findOneByUsername(anyString())).thenReturn(user);

        User foundUser = userService.findUserByUsername(USERNAME);

        assertEquals(user, foundUser);
    }

    @Test
    public void findUserByLoginShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> userService.findUserByUsername(""));
    }

    @Test
    public void getUserAuthorityShouldReturnExpectedAuthority() {
        String expectedAuthority = "USER_ROLE";
        when(userRepository.findAuthorityByUsername(USERNAME)).thenReturn(expectedAuthority);

        String authority = userService.getUserAuthority(USERNAME);

        assertEquals(expectedAuthority, authority);
    }

    @Test
    public void getUserAuthorityShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> userService.getUserAuthority(""));
    }
}
