package kz.zhanbolat.university.lecturer.entity;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.degree.entity.AcademicDegree;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.user.entity.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "lecturer")
public class Lecturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String fio;
    private LocalDate birthday;
    private String phone;
    private String address;
    private String email;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "academic_degree_name")
    private AcademicDegree academicDegree;

    public Lecturer() {
    }

    public Lecturer(Builder builder) {
        this.id = builder.id;
        this.fio = builder.fio;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.address = builder.address;
        this.email = builder.email;
        this.user = builder.user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AcademicDegree getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(AcademicDegree academicDegree) {
        this.academicDegree = academicDegree;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("fio", fio)
                .append("birthday", birthday)
                .append("phone", phone)
                .append("address", address)
                .append("email", email)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Lecturer lecturer = (Lecturer) o;

        return new EqualsBuilder()
                .append(id, lecturer.id)
                .append(fio, lecturer.fio)
                .append(birthday, lecturer.birthday)
                .append(phone, lecturer.phone)
                .append(address, lecturer.address)
                .append(email, lecturer.email)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(fio)
                .append(birthday)
                .append(phone)
                .append(address)
                .append(email)
                .toHashCode();
    }

    public static class Builder {
        private Integer id;
        private String fio;
        private LocalDate birthday;
        private String phone;
        private String address;
        private String email;
        private User user;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder fio(String fio) {
            this.fio = fio;

            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;

            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;

            return this;
        }

        public Builder address(String address) {
            this.address = address;

            return this;
        }

        public Builder email(String email) {
            this.email = email;

            return this;
        }

        public Builder user(User user) {
            this.user = user;

            return this;
        }

        public Lecturer build() {
            return new Lecturer(this);
        }
    }
}
