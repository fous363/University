package kz.zhanbolat.university.schedule.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ScheduleCreateForm {
    private String day;
    private String time;
    private String course;
    private String location;

    public ScheduleCreateForm() {
    }

    public ScheduleCreateForm(Builder builder) {
        this.day = builder.day;
        this.time = builder.time;
        this.course = builder.course;
        this.location = builder.location;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String day;
        private String time;
        private String course;
        private String location;

        public Builder day(String day) {
            this.day = day;

            return this;
        }

        public Builder time(String time) {
            this.time = time;

            return this;
        }

        public Builder course(String course) {
            this.course = course;

            return this;
        }

        public Builder location(String location) {
            this.location = location;

            return this;
        }

        public ScheduleCreateForm build() {
            return new ScheduleCreateForm(this);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("day", day)
                .append("time", time)
                .append("course", course)
                .append("location", location)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ScheduleCreateForm that = (ScheduleCreateForm) o;

        return new EqualsBuilder()
                .append(day, that.day)
                .append(time, that.time)
                .append(course, that.course)
                .append(location, that.location)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(day)
                .append(time)
                .append(course)
                .append(location)
                .toHashCode();
    }
}
