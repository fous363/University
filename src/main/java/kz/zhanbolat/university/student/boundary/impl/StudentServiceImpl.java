package kz.zhanbolat.university.student.boundary.impl;

import kz.zhanbolat.university.course.boundary.CourseService;
import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.group.entity.Group;
import kz.zhanbolat.university.lecturer.boundary.LecturerService;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.control.StudentRepository;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private LecturerService lecturerService;
    @Autowired
    private CourseService courseService;

    @Override
    public Student getStudentByUsername(String username) {
        if (username.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return studentRepository.findOneByUsername(username);
    }

    @Override
    public void registerNewStudent(Student student) {
        if (Objects.isNull(student.getBirthday())) {
            throw new IllegalArgumentException();
        }
        studentRepository.create(student);
    }

    @Override
    public void updateStudentInfo(Student student, String username) {
        if (Objects.isNull(student.getBirthday())) {
            throw new IllegalArgumentException();
        }
        studentRepository.updateStudentInfoByUsername(student, username);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudent(Integer studentId) {
        return studentRepository.findOne(studentId);
    }

    @Override
    public void assignStudentToGroup(Student student, Group group) {
        studentRepository.updateStudentGroup(student.getId(), group.getId());
    }

    @Override
    public List<Student> getAllStudents(String lecturerUsername) {
        final List<Course> courses = courseService.getAllUserCourses(lecturerUsername);
        return studentRepository.findAllByCourses(courses);
    }

    @Override
    public List<Student> getAllStudents(Integer groupId) {
        return studentRepository.findAllByGroup(groupId);
    }

    @Override
    public List<Student> getAllStudentsBySchedule(Integer scheduleId) {
        return studentRepository.findAllBySchedule(scheduleId);
    }
}
