package kz.zhanbolat.university.student.entity;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.group.entity.Group;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.semester.entity.Semester;
import kz.zhanbolat.university.user.entity.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String fio;
    private LocalDate birthday;
    private String phone;
    private String address;
    private String email;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany
    @JoinTable(name = "student_semester",
               joinColumns = @JoinColumn(name = "student_id"),
               inverseJoinColumns = @JoinColumn(name = "semester_id"))
    private List<Semester> semesters;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    public Student() {
    }

    public Student(Builder builder) {
        this.id = builder.id;
        this.fio = builder.fio;
        this.birthday = builder.birthday;
        this.phone = builder.phone;
        this.address = builder.address;
        this.email = builder.email;
        this.user = builder.user;
        this.group = builder.group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<Semester> semesters) {
        this.semesters = semesters;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("fio", fio)
                .append("birthday", birthday)
                .append("phone", phone)
                .append("address", address)
                .append("email", email)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return new EqualsBuilder()
                .append(id, student.id)
                .append(fio, student.fio)
                .append(birthday, student.birthday)
                .append(phone, student.phone)
                .append(address, student.address)
                .append(email, student.email)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(fio)
                .append(birthday)
                .append(phone)
                .append(address)
                .append(email)
                .toHashCode();
    }

    public static class Builder {
        private Integer id;
        private String fio;
        private LocalDate birthday;
        private String phone;
        private String address;
        private String email;
        private User user;
        private Group group;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder fio(String fio) {
            this.fio = fio;

            return this;
        }

        public Builder birthday(LocalDate birthday) {
            this.birthday = birthday;

            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;

            return this;
        }

        public Builder address(String address) {
            this.address = address;

            return this;
        }

        public Builder email(String email) {
            this.email = email;

            return this;
        }

        public Builder user(User user) {
            this.user = user;

            return this;
        }

        public Builder group(Group group) {
            this.group = group;

            return this;
        }

        public Student build() {
            return new Student(this);
        }
    }
}
