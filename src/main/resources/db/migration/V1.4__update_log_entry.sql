ALTER TABLE log_entry
DROP log_status;

ALTER TABLE log_entry
ADD logged_in boolean not null;

ALTER TABLE log_entry
ADD logged_off boolean not null;