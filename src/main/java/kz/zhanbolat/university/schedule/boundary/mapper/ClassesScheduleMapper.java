package kz.zhanbolat.university.schedule.boundary.mapper;

import kz.zhanbolat.university.course.boundary.CourseService;
import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.location.boundary.LocationService;
import kz.zhanbolat.university.location.entity.Location;
import kz.zhanbolat.university.schedule.boundary.dto.ClassesScheduleForm;
import kz.zhanbolat.university.schedule.boundary.dto.ScheduleCreateForm;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.semester.entity.Semester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public class ClassesScheduleMapper {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    private static final int AUDIENCE_POSITION = 0;
    private static final int CAMPUS_POSITION = 1;
    @Autowired
    private LocationService locationService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SemesterService semesterService;

    public ClassesScheduleForm map(ClassesSchedule source) {
        String location = new StringBuilder()
                .append(source.getLocation().getAudience())
                .append(", ")
                .append(source.getLocation().getCampus())
                .toString();
        return ClassesScheduleForm.builder()
                .id(source.getId())
                .dateTime(source.getDateTime().format(formatter))
                .courseName(source.getCourse().getName())
                .location(location)
                .build();
    }

    public ClassesSchedule map(ScheduleCreateForm source) {
        ClassesSchedule classesSchedule = new ClassesSchedule();
        classesSchedule.setCourse(courseService.getCourse(source.getCourse()));
        String[] locations = source.getLocation().split(", ");
        classesSchedule.setLocation(locationService.getLocation(locations[AUDIENCE_POSITION], locations[CAMPUS_POSITION]));
        classesSchedule.setDateTime(LocalDateTime.of(LocalDate.parse(source.getDay(), DateTimeFormatter.ISO_DATE),
                LocalTime.parse(source.getTime(), DateTimeFormatter.ISO_TIME)));
        return classesSchedule;
    }
}
