package kz.zhanbolat.university.log.entity.event;

import kz.zhanbolat.university.log.entity.LogStatus;
import org.springframework.context.ApplicationEvent;

public class LogEvent extends ApplicationEvent {
    private String username;
    private LogStatus logStatus;

    public LogEvent(Object source, String username, LogStatus logStatus) {
        super(source);
        this.username = username;
        this.logStatus = logStatus;
    }

    public String getUsername() {
        return username;
    }

    public LogStatus getLogStatus() {
        return logStatus;
    }
}
