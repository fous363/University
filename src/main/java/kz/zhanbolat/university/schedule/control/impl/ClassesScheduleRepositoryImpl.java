package kz.zhanbolat.university.schedule.control.impl;

import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.schedule.control.ClassesScheduleRepository;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.entity.Student;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@Repository
public class ClassesScheduleRepositoryImpl implements ClassesScheduleRepository {
    private static final String SELECT_ALL_BY_STUDENT_USER_ID = "select classes_schedule.id, classes_schedule.date_and_time, " +
            "classes_schedule.location_id, classes_schedule.course_id from classes_schedule " +
            "inner join user_classes_schedule on user_classes_schedule.user_id = :user_id " +
            "and user_classes_schedule.classes_schedule_id = classes_schedule.id " +
            "inner join student on user_classes_schedule.user_id = student.user_id";
    private static final String COUNT_SCHEDULE_OF_DATE_TIME = "select count(classes_schedule.id) from classes_schedule " +
            "inner join user_classes_schedule on user_classes_schedule.user_id = :user_id " +
            "and user_classes_schedule.classes_schedule_id = classes_schedule.id " +
            "where date_and_time=:date_and_time";
    private static final String INSERT_USER_SCHEDULE =
            "insert into user_classes_schedule(user_id, classes_schedule_id) values (:user_id, :classes_schedule_id)";
    private static final String SELECT_ALL_BY_LECTURER_USER_ID = "select classes_schedule.id, classes_schedule.date_and_time, " +
            "classes_schedule.location_id, classes_schedule.course_id from classes_schedule " +
            "inner join user_classes_schedule on user_classes_schedule.user_id = :user_id " +
            "and user_classes_schedule.classes_schedule_id = classes_schedule.id " +
            "inner join lecturer on user_classes_schedule.user_id = lecturer.user_id";;
    private static final String UPDATE_MARK = "update user_classes_schedule set mark=:mark where user_id=:user_id " +
            "and classes_schedule_id = :classes_schedule_id";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createUserSchedule(ClassesSchedule schedule, Integer userId) {
        if (Objects.isNull(schedule.getId())) {
            entityManager.persist(schedule);
        }
        Query query = entityManager.createNativeQuery(INSERT_USER_SCHEDULE);
        query.setParameter("user_id", userId);
        query.setParameter("classes_schedule_id", schedule.getId());
        query.executeUpdate();
    }

    @Override
    public boolean isUserDateTimeEngaged(ClassesSchedule schedule, Integer userId) {
        Query query = entityManager.createNativeQuery(COUNT_SCHEDULE_OF_DATE_TIME);
        query.setParameter("date_and_time", schedule.getDateTime());
        query.setParameter("user_id", userId);
        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }

    @Override
    public List<ClassesSchedule> findAllByLecturerUserId(Integer userId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_LECTURER_USER_ID, ClassesSchedule.class);
        query.setParameter("user_id", userId);
        return query.getResultList();
    }

    @Override
    public List<ClassesSchedule> findAllByStudentUserId(Integer userId) {
        Query query = entityManager.createNativeQuery(SELECT_ALL_BY_STUDENT_USER_ID, ClassesSchedule.class);
        query.setParameter("user_id", userId);
        return query.getResultList();
    }

    @Override
    public ClassesSchedule findOne(Integer scheduleId) {
        return entityManager.find(ClassesSchedule.class, scheduleId);
    }

    @Override
    public void updateScheduleMark(Student student, ClassesSchedule schedule, Integer mark) {
        final Query query = entityManager.createNativeQuery(UPDATE_MARK);
        query.setParameter("user_id", student.getUser().getId());
        query.setParameter("classes_schedule_id", schedule.getId());
        query.setParameter("mark", mark);
        query.executeUpdate();
    }
}
