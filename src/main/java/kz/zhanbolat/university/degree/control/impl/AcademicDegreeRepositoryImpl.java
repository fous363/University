package kz.zhanbolat.university.degree.control.impl;

import kz.zhanbolat.university.degree.control.AcademicDegreeRepository;
import kz.zhanbolat.university.degree.entity.AcademicDegree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AcademicDegreeRepositoryImpl implements AcademicDegreeRepository {
    private static final String SELECT_ALL = "SELECT name, priority FROM academic_degree";
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<AcademicDegree> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, AcademicDegree.class);
        return query.getResultList();
    }

    @Override
    public AcademicDegree findOne(String name) {
        return entityManager.find(AcademicDegree.class, name);
    }
}
