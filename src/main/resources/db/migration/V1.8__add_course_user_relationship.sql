DROP TABLE student_course;
DROP TABLE lecturer_course;

CREATE TABLE user_course(
    user_id int not null,
    course_code char(10) not null,
    foreign key (user_id) references user_account(id),
    foreign key (course_code) references course(code)
);