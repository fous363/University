package kz.zhanbolat.university.log.boundary;

import kz.zhanbolat.university.log.boundary.dto.LogExportForm;
import kz.zhanbolat.university.log.boundary.mapper.LogMapper;
import kz.zhanbolat.university.log.boundary.view.LogXlsxView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;
    @Autowired
    private LogMapper mapper;

    @GetMapping(value = "/admin/export")
    public ModelAndView export() {
        List<LogExportForm> logs = logService.getAllLogs().stream().map(log -> mapper.map(log)).collect(Collectors.toList());
        return new ModelAndView(new LogXlsxView(), "logs", logs);
    }
}
