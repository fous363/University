package kz.zhanbolat.university.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Configuration
public class EncryptionConfig {
    private static final String ALGORITHM = "SHA-256";

    @Bean
    public MessageDigest messageDigest() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(ALGORITHM);
    }
}
