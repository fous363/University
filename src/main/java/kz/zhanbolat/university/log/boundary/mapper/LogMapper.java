package kz.zhanbolat.university.log.boundary.mapper;

import kz.zhanbolat.university.log.boundary.dto.LogExportForm;
import kz.zhanbolat.university.log.entity.Log;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;

@Component
public class LogMapper {

    public LogExportForm map(Log source) {
        return LogExportForm.builder()
                .username(source.getUser().getUsername().trim())
                .loggedIn(source.isLoggedIn())
                .loggedOff(source.isLoggedOff())
                .timestamp(source.getTimeStamp().format(DateTimeFormatter.ISO_DATE_TIME))
                .build();
    }
}
