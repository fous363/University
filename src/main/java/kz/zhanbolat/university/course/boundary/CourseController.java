package kz.zhanbolat.university.course.boundary;

import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.course.boundary.dto.CourseForm;
import kz.zhanbolat.university.course.boundary.mapper.CourseMapper;
import kz.zhanbolat.university.course.entity.Course;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/course")
public class CourseController {
    private static final Logger logger = LogManager.getLogger(CourseController.class);
    @Autowired
    private CourseService courseService;

    @GetMapping("/student")
    public String studentCoursePage(ModelMap modelMap, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        List<CourseForm> courses = courseService.getAllUserCourses(username).stream().map(CourseMapper::map).collect(Collectors.toList());
        modelMap.addAttribute("courses", courses);
        return "studentCourse";
    }

    @GetMapping("/lecturer")
    public String lecturerCoursePage(ModelMap modelMap, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        List<CourseForm> courses = courseService.getAllUserCourses(username).stream().map(CourseMapper::map).collect(Collectors.toList());
        modelMap.addAttribute("courses", courses);
        return "lecturerCourse";
    }

    @GetMapping("/admin")
    public String adminCoursePage(ModelMap model) {
        List<Course> courses = courseService.getAllCourses();
        model.addAttribute("courses", courses.stream().map(CourseMapper::map).collect(Collectors.toList()));
        return "adminCourse";
    }

    @GetMapping("/admin/create")
    public String adminCourseCreatePage(ModelMap model) {
        model.addAttribute("course", new CourseForm());
        return "adminCreateCourse";
    }

    @PostMapping("/admin/create")
    public String processCourseCreation(@ModelAttribute(name = "course") CourseForm courseForm, ModelMap model) {
        try {
            courseService.createCourse(CourseMapper.map(courseForm));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminCourseCreatePage(model);
        }
        return "redirect:/course/admin";
    }

    @GetMapping("/admin/delete/{course_code}")
    public String processDeleteCourse(@PathVariable(name = "course_code")String courseCode) {
        courseService.deleteCourse(courseCode);
        return "redirect:/course/admin";
    }

    @GetMapping("/admin/update/{course_code}")
    public String adminCourseUpdatePage(@PathVariable(name = "course_code")String courseCode, ModelMap model) {
        model.addAttribute("course", CourseMapper.map(courseService.getCourse(courseCode)));
        return "adminUpdateCourse";
    }

    @PostMapping("/admin/update")
    public String processUpdateCourse(@ModelAttribute("course") CourseForm course) {
        courseService.updateCourse(CourseMapper.map(course));
        return "redirect:/course/admin";
    }
}
