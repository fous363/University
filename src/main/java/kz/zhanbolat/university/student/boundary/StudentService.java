package kz.zhanbolat.university.student.boundary;

import kz.zhanbolat.university.group.entity.Group;
import kz.zhanbolat.university.student.entity.Student;

import java.util.List;

public interface StudentService {
    Student getStudentByUsername(String username);
    void registerNewStudent(Student student);
    void updateStudentInfo(Student student, String username);
    List<Student> getAllStudents();
    Student getStudent(Integer studentId);
    void assignStudentToGroup(Student student, Group group);
    List<Student> getAllStudents(String lecturerUsername);
    List<Student> getAllStudents(Integer groupId);
    List<Student> getAllStudentsBySchedule(Integer scheduleId);
}
