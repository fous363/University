package kz.zhanbolat.university.location.boundary;

import kz.zhanbolat.university.location.entity.Location;

import java.util.List;

public interface LocationService {
    List<Location> getAllLocations();
    Location getLocation(Integer locationId);
    void createLocation(Location location);
    void updateLocation(Location location);
    void deleteLocation(Integer locationId);
    Location getLocation(String audience, String campus);
}
