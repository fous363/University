package kz.zhanbolat.university.course.entity;

import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.entity.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    @Column(name = "name")
    private String name;
    private String description;

    @ManyToMany
    @JoinTable(name = "user_course",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private List<User> users;

    @OneToMany(mappedBy = "course", targetEntity = ClassesSchedule.class)
    private List<ClassesSchedule> schedules;

    public Course() {
    }

    public Course(Builder builder) {
        this.id = builder.id;
        this.code = builder.code;
        this.name = builder.name;
        this.description = builder.description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ClassesSchedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ClassesSchedule> schedules) {
        this.schedules = schedules;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("code", code)
                .append("name", name)
                .append("description", description)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return new EqualsBuilder()
                .append(id, course.id)
                .append(code, course.code)
                .append(name, course.name)
                .append(description, course.description)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(code)
                .append(name)
                .append(description)
                .toHashCode();
    }

    public static class Builder {
        private Integer id;
        private String code;
        private String name;
        private String description;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;

            return this;
        }

        public Builder code(String code) {
            this.code = code;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder description(String description) {
            this.description = description;

            return this;
        }

        public Course build() {
            return new Course(this);
        }
    }
}
