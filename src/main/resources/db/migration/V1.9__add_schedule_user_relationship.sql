DROP TABLE student_classes_schedule;
DROP TABLE lecturer_classes_schedule;

CREATE TABLE user_classes_schedule(
    user_id int not null,
    classes_schedule_id int not null,
    foreign key (user_id) references user_account(id),
    foreign key (classes_schedule_id) references classes_schedule(id)
);
