package kz.zhanbolat.university.user.boundary.mapper;

import kz.zhanbolat.university.user.boundary.dto.UserForm;
import kz.zhanbolat.university.user.boundary.dto.UserRegistrationForm;
import kz.zhanbolat.university.user.entity.User;

public final class UserMapper {

    public static User map(UserRegistrationForm source) {
        return User.builder().username(source.getUsername()).password(source.getPassword()).build();
    }

    public static UserForm map(User user) {
        return new UserForm(user.getUsername(), user.getPassword(), user.getActive());
    }
}
