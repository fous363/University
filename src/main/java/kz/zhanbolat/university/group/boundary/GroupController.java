package kz.zhanbolat.university.group.boundary;

import kz.zhanbolat.university.group.boundary.dto.GroupForm;
import kz.zhanbolat.university.group.boundary.mapper.GroupMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/group")
public class GroupController {
    private static final Logger logger = LogManager.getLogger(GroupController.class);
    @Autowired
    private GroupService groupService;

    @GetMapping("/admin")
    public String adminGroupPage(ModelMap model) {
        model.addAttribute("groups",
                groupService.getAllGroups().stream().map(GroupMapper::map).collect(Collectors.toList()));
        return "adminGroups";
    }

    @GetMapping("/admin/create")
    public String adminCreateGroupPage(ModelMap model) {
        model.addAttribute("group", new GroupForm());
        return "adminCreateGroup";
    }

    @GetMapping("/admin/update/{group_id}")
    public String adminUpdateGroupPage(@PathVariable("group_id") Integer groupId, ModelMap model) {
        model.addAttribute("group", GroupMapper.map(groupService.getGroup(groupId)));
        return "adminUpdateGroup";
    }

    @PostMapping("/admin/create")
    public String processAdminCreate(@ModelAttribute("group") GroupForm group, ModelMap model) {
        try {
            groupService.createGroup(GroupMapper.map(group));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminCreateGroupPage(model);
        }
        return "redirect:/group/admin";
    }

    @PostMapping("/admin/update")
    public String processAdminUpdate(@ModelAttribute("group") GroupForm group, ModelMap model) {
        try {
            groupService.updateGroup(GroupMapper.map(group));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminUpdateGroupPage(group.getId(), model);
        }
        return "redirect:/group/admin";
    }

    @GetMapping("/admin/delete/{group_id}")
    public String processAdminDelete(@PathVariable("group_id")Integer groupId) {
        groupService.deleteGroup(groupId);
        return "redirect:/group/admin";
    }
}
