CREATE TABLE academic_degree(
    name char(40) primary key not null,
    priority smallint
);

INSERT INTO academic_degree(name, priority) VALUES ('BACHELAR', 1);
INSERT INTO academic_degree(name, priority) VALUES ('MASTER', 2);
INSERT INTO academic_degree(name, priority) VALUES ('Ph.D', 3);

CREATE TABLE lecturer(
    id serial primary key,
    fio varchar(120) not null,
    birthday date,
    phone char(16),
    address varchar(80),
    email varchar(80),
    user_id int not null,
    academic_degree_name char(40) not null,
    foreign key (user_id) references user_account(id),
    foreign key (academic_degree_name) references academic_degree(name)
);