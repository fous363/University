package kz.zhanbolat.university.course.boundary.mapper;

import com.google.common.base.Strings;
import kz.zhanbolat.university.course.boundary.dto.CourseForm;
import kz.zhanbolat.university.course.entity.Course;

import java.util.Optional;

public class CourseMapper {

    public static CourseForm map(Course source) {
        return CourseForm.builder().id(String.valueOf(source.getId())).code(source.getCode().trim())
                .name(source.getName().trim())
                .description(Optional.ofNullable(source.getDescription()).orElse("").trim()).build();
    }

    public static Course map(CourseForm source) {
        Course course = Course.builder()
                .code(source.getCode())
                .name(source.getName())
                .description(source.getDescription())
                .build();
        if (!Strings.isNullOrEmpty(source.getId())) {
            course.setId(Integer.parseInt(source.getId()));
        }
        return course;
    }
}
