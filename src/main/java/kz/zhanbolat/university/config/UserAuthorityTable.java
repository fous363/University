package kz.zhanbolat.university.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum UserAuthorityTable {
    INSTANCE;

    private Map<String, UserAuthority> tokenAuthorityMap = new ConcurrentHashMap<>();

    public void put(String token, String username, String authorityName) {
        tokenAuthorityMap.put(token, new UserAuthority(username, authorityName));
    }

    public void remove(String token) {
        tokenAuthorityMap.remove(token);
    }

    public String getAuthorityName(String token) {
        return tokenAuthorityMap.get(token).getAuthorityName();
    }

    public String getUsername(String token) {
        return tokenAuthorityMap.get(token).getUsername();
    }

    private static class UserAuthority {
        private String username;
        private String authorityName;

        public UserAuthority() {
        }

        public UserAuthority(String username, String authorityName) {
            this.username = username;
            this.authorityName = authorityName;
        }

        public String getAuthorityName() {
            return authorityName;
        }

        public String getUsername() {
            return username;
        }
    }
}
