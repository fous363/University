package kz.zhanbolat.university.schedule.boundary;

import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.course.boundary.CourseService;
import kz.zhanbolat.university.course.boundary.mapper.CourseMapper;
import kz.zhanbolat.university.location.boundary.LocationService;
import kz.zhanbolat.university.location.boundary.mapper.LocationMapper;
import kz.zhanbolat.university.schedule.boundary.dto.ClassesScheduleForm;
import kz.zhanbolat.university.schedule.boundary.dto.Mark;
import kz.zhanbolat.university.schedule.boundary.dto.ScheduleCreateForm;
import kz.zhanbolat.university.schedule.boundary.mapper.ClassesScheduleMapper;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/schedule")
public class ClassesScheduleController {
    @Autowired
    private ClassesScheduleService scheduleService;
    @Autowired
    private ClassesScheduleMapper scheduleMapper;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private UserService userService;

    @GetMapping("/student")
    public String studentClassesSchedulePage(ModelMap model, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        List<ClassesSchedule> schedules =
                scheduleService.getStudentUserClassesSchedule(userService.findUserByUsername(username).getId());
        List<ClassesScheduleForm> scheduleForms =
                schedules.stream().map((scheduleMapper::map)).collect(Collectors.toList());
        model.addAttribute("classesSchedules", scheduleForms);
        return "studentClassesSchedule";
    }

    @GetMapping("/lecturer")
    public String lecturerClassesSchedulePage(ModelMap model, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        List<ClassesSchedule> schedules =
                scheduleService.getLecturerUserClassesSchedule(userService.findUserByUsername(username).getId());
        List<ClassesScheduleForm> scheduleForms =
                schedules.stream().map((scheduleMapper::map)).collect(Collectors.toList());
        model.addAttribute("classesSchedules", scheduleForms);
        return "lecturerClassesSchedule";
    }

    @GetMapping("/lecturer/create/{student_id}")
    public String lecturerStudentClassesScheduleCreatePage(@PathVariable("student_id")Integer studentId,
                                                           ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        model.addAttribute("courses",
                courseService.getAllUserCourses(username).stream().map(CourseMapper::map).collect(Collectors.toList()));
        model.addAttribute("locations",
                locationService.getAllLocations().stream().map(LocationMapper::map).collect(Collectors.toList()));
        model.addAttribute("student_id", studentId);
        model.addAttribute("schedule", new ScheduleCreateForm());
        return "lecturerStudentClassesScheduleCreate";
    }

    @PostMapping("/lecturer/create/{student_id}")
    @Transactional(rollbackOn = RuntimeException.class)
    public String processLecturerStudentClassesScheduleCreate(@PathVariable("student_id")Integer studentId,
                                                              @ModelAttribute("schedule") ScheduleCreateForm schedule,
                                                              HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        ClassesSchedule classesSchedule = scheduleMapper.map(schedule);
        scheduleService.createUserSchedule(classesSchedule, userService.getUserByStudent(studentId).getId());
        scheduleService.createUserSchedule(classesSchedule, userService.findUserByUsername(username).getId());
        return "redirect:/schedule/lecturer";
    }

    @GetMapping("/lecturer/create/group/{group_id}")
    public String lecturerGroupClassesScheduleCreatePage(@PathVariable("group_id")Integer groupId,
                                                         ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        model.addAttribute("courses",
                courseService.getAllUserCourses(username).stream().map(CourseMapper::map).collect(Collectors.toList()));
        model.addAttribute("locations",
                locationService.getAllLocations().stream().map(LocationMapper::map).collect(Collectors.toList()));
        model.addAttribute("group_id", groupId);
        model.addAttribute("schedule", new ScheduleCreateForm());
        return "lecturerGroupClassesScheduleCreate";
    }

    @PostMapping("/lecturer/create/group/{group_id}")
    @Transactional(rollbackOn = RuntimeException.class)
    public String processLecturerGroupClassesScheduleCreate(@PathVariable("group_id")Integer groupId,
                                                            @ModelAttribute("schedule") ScheduleCreateForm schedule,
                                                            HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        ClassesSchedule classesSchedule = scheduleMapper.map(schedule);
        scheduleService.createGroupSchedule(classesSchedule, groupId);
        scheduleService.createUserSchedule(classesSchedule, userService.findUserByUsername(username).getId());
        return "redirect:/schedule/lecturer";
    }

    @GetMapping("/lecturer/mark/{student_id}/{schedule_id}")
    public String lecturerMarkStudent(@PathVariable("student_id") Integer studentId,
                                      @PathVariable("schedule_id") Integer scheduleId, ModelMap model) {
        model.addAttribute("mark", new Mark());
        model.addAttribute("student_id", studentId);
        model.addAttribute("schedule_id", scheduleId);
        return "lecturerMarkStudent";
    }

    @PostMapping("/lecturer/mark/{student_id}/{schedule_id}")
    public String processLecturerMarkStudent(@PathVariable("student_id") Integer studentId,
                                             @PathVariable("schedule_id") Integer scheduleId,
                                             @ModelAttribute("mark") Mark mark) {
        scheduleService.markStudent(studentService.getStudent(studentId), scheduleService.getSchedule(scheduleId), mark.getValue());
        return "redirect:/student/lecturer/mark/" + scheduleId;
    }
}
