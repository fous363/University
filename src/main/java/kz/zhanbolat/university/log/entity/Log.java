package kz.zhanbolat.university.log.entity;

import kz.zhanbolat.university.user.entity.User;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "log_entry")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "logged_in")
    private boolean loggedIn;
    @Column(name = "logged_off")
    private boolean loggedOff;
    @Column(name = "date_and_time")
    private LocalDateTime timeStamp;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    public Log() {
    }

    public Log(Builder builder) {
        this.id = builder.id;
        this.timeStamp = builder.timeStamp;
        this.loggedIn = builder.loggedIn;
        this.loggedOff = builder.loggedOff;
        this.user = builder.user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isLoggedOff() {
        return loggedOff;
    }

    public void setLoggedOff(boolean loggedOff) {
        this.loggedOff = loggedOff;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("loggedIn", loggedIn)
                .append("loggedOff", loggedOff)
                .append("timeStamp", timeStamp)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Log log = (Log) o;

        return new EqualsBuilder()
                .append(loggedIn, log.loggedIn)
                .append(loggedOff, log.loggedOff)
                .append(id, log.id)
                .append(timeStamp, log.timeStamp)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(loggedIn)
                .append(loggedOff)
                .append(timeStamp)
                .toHashCode();
    }

    public static Builder builder() {
        return new Log.Builder();
    }

    public static class Builder {
        private Long id;
        private boolean loggedIn;
        private boolean loggedOff;
        private LocalDateTime timeStamp;
        private User user;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;

            return this;
        }

        public Builder loggedIn(boolean loggedIn) {
            this.loggedIn = loggedIn;

            return this;
        }

        public Builder loggedOff(boolean loggedOff) {
            this.loggedOff = loggedOff;

            return this;
        }

        public Builder timeStamp(LocalDateTime timeStamp) {
            this.timeStamp = timeStamp;

            return this;
        }

        public Builder user(User user) {
            this.user = user;

            return this;
        }

        public Log build() {
            return new Log(this);
        }
    }
}
