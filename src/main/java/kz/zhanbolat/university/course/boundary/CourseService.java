package kz.zhanbolat.university.course.boundary;

import kz.zhanbolat.university.course.entity.Course;

import java.util.List;

public interface CourseService {
    List<Course> getAllUserCourses(String username);
    void createCourse(Course course);
    void deleteCourse(String courseCode);
    List<Course> getAllCourses();
    Course getCourse(String courseCode);
    void updateCourse(Course course);
}
