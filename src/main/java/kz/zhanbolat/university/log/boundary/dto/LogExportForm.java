package kz.zhanbolat.university.log.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class LogExportForm {
    private String username;
    private String timestamp;
    private boolean loggedIn;
    private boolean loggedOff;

    public LogExportForm() {
    }

    public LogExportForm(Builder builder) {
        this.username = builder.username;
        this.timestamp = builder.timestamp;
        this.loggedIn = builder.loggedIn;
        this.loggedOff = builder.loggedOff;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isLoggedOff() {
        return loggedOff;
    }

    public void setLoggedOff(boolean loggedOff) {
        this.loggedOff = loggedOff;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String username;
        private String timestamp;
        private boolean loggedIn;
        private boolean loggedOff;

        private Builder() {
        }

        public Builder username(String username) {
            this.username = username;

            return this;
        }

        public Builder timestamp(String timestamp) {
            this.timestamp = timestamp;

            return this;
        }

        public Builder loggedIn(boolean loggedIn) {
            this.loggedIn = loggedIn;

            return this;
        }

        public Builder loggedOff(boolean loggedOff) {
            this.loggedOff = loggedOff;

            return this;
        }

        public LogExportForm build() {
            return new LogExportForm(this);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("username", username)
                .append("timestamp", timestamp)
                .append("loggedIn", loggedIn)
                .append("loggedOff", loggedOff)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LogExportForm that = (LogExportForm) o;

        return new EqualsBuilder()
                .append(loggedIn, that.loggedIn)
                .append(loggedOff, that.loggedOff)
                .append(username, that.username)
                .append(timestamp, that.timestamp)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(username)
                .append(timestamp)
                .append(loggedIn)
                .append(loggedOff)
                .toHashCode();
    }
}
