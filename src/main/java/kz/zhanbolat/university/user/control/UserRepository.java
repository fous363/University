package kz.zhanbolat.university.user.control;

import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.user.entity.User;

import java.util.List;

public interface UserRepository {
    User findOneByUsername(String username);
    boolean checkUserExists(String username, String password);
    void create(User user);
    String findAuthorityByUsername(String username);
    void updatePassword(String username, String password);
    void updateUserAuthority(String username, String authority);
    List<User> findAllWithExcludeOne(User user);
    void deleteByUsername(String username);
    void updateUserActiveStatus(String username, boolean activeStatus);
    void findAllStudentUsersByCoursesAndLecturer(List<Course> courses, String username);
    User findOneByStudentId(Integer studentId);
}
