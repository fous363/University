package kz.zhanbolat.university.user.boundary;

import kz.zhanbolat.university.config.exception.InternalServerException;
import kz.zhanbolat.university.log.boundary.LogEventPublisher;
import kz.zhanbolat.university.log.entity.LogStatus;
import kz.zhanbolat.university.user.boundary.dto.UserForm;
import kz.zhanbolat.university.user.boundary.dto.UserRegistrationForm;
import kz.zhanbolat.university.user.boundary.mapper.UserMapper;
import kz.zhanbolat.university.user.control.TokenGenerator;
import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.user.control.UserRestrictions;
import kz.zhanbolat.university.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private LogEventPublisher publisher;
    @Autowired
    private TokenGenerator tokenGenerator;

    @GetMapping("/login")
    public String loginPage(ModelMap model) {
        model.addAttribute("user", new UserForm());
        return "login";
    }

    @PostMapping("/login")
    public String processLogin(@ModelAttribute("user") UserForm user, ModelMap modelMap, HttpSession session) {
        if (userService.authenticateUser(user.getUsername(), user.getPassword())) {
            publisher.publishLogEvent(user.getUsername(), LogStatus.LOG_IN);
            String token = tokenGenerator.generateToken(user.getUsername(), user.getPassword());
            String authorityName = userService.getUserAuthority(user.getUsername()).trim();
            UserAuthorityTable.INSTANCE.put(token, user.getUsername(), authorityName);
            session.setAttribute("access-token", token);
            if (modelMap.containsAttribute("loginError")) {
                modelMap.remove("loginError");
            }
            if (Objects.equals(authorityName, "USER_STUDENT")) {
                return "redirect:/student/profile";
            } else if (Objects.equals(authorityName, "USER_LECTURER")) {
                return "redirect:/lecturer/profile";
            } else if (Objects.equals(authorityName, "USER_ADMIN")) {
                return "redirect:/user/profile/admin";
            }
        }
        modelMap.addAttribute("loginError", "Incorrect username or password.");
        return loginPage(modelMap);
    }

    @GetMapping("/singUp")
    public String singUpPage(ModelMap model) {
        model.addAttribute("user", new UserRegistrationForm());
        return "userSingUp";
    }

    @PostMapping("/singUp")
    public String singUp(@ModelAttribute("user") UserRegistrationForm user, HttpSession session, ModelMap model) {
        try {
            userService.registerNewUser(UserMapper.map(user));
        } catch (IllegalArgumentException e) {
            model.addAttribute("userSignUpError", e.getMessage());
            return singUpPage(model);
        }
        session.setAttribute("username", user.getUsername());
        if (Objects.equals(user.getSingUpAs(), "student")) {
            userService.updateUserAuthority(user.getUsername(), "USER_STUDENT");
            return "redirect:/student/singUp";
        } else if (Objects.equals(user.getSingUpAs(), "lecturer")) {
            userService.updateUserAuthority(user.getUsername(), "USER_LECTURER");
            return "redirect:/lecturer/singUp";
        }
        return singUpPage(model);
    }

    @GetMapping("/logout")
    public RedirectView logout(HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        publisher.publishLogEvent(username, LogStatus.LOG_OUT);
        UserAuthorityTable.INSTANCE.remove(token);
        session.removeAttribute(token);
        return new RedirectView("/university/");
    }

    @GetMapping("/changePassword")
    public String changePasswordPage(ModelMap model, HttpSession session) {
        model.addAttribute("user", new UserForm());
        final String authorityName = UserAuthorityTable.INSTANCE
                .getAuthorityName((String) session.getAttribute("access-token"));
        if ("USER_STUDENT".equals(authorityName)) {
            model.addAttribute("profile_url", "/student/profile");
        } else if ("USER_LECTURER".equals(authorityName)) {
            model.addAttribute("profile_url", "/lecturer/profile");
        } else if ("USER_ADMIN".equals(authorityName)) {
            model.addAttribute("profile_url", "/user/profile/admin");
        }
        return "changePasswordPage";
    }

    @PostMapping("/changePassword")
    public String changePassword(UserForm user, ModelMap model, HttpSession httpSession) {
        String token = (String) httpSession.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        userService.changePassword(username, user.getPassword());
        return changePasswordPage(model, httpSession);
    }

    @GetMapping("/profile/admin")
    public String adminProfilePage(ModelMap model, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        User user = userService.findUserByUsername(username);
        model.addAttribute("user", UserMapper.map(user));
        return "adminProfile";
    }

    @GetMapping("/admin/users")
    public String adminUsersPage(ModelMap model, HttpSession session) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        User user = userService.findUserByUsername(username);
        List<User> users = userService.getAllUsersWithRestriction(UserRestrictions.EXCLUDE_ONE, Collections.singletonList(user));
        model.addAttribute("users", users.stream().map(UserMapper::map).collect(Collectors.toList()));
        return "adminUsers";
    }

    @GetMapping("/admin/delete/{username}")
    public String processDeleteUser(@PathVariable(name = "username") String username) {
        userService.deleteUser(username);
        return "redirect:/user/admin/users";
    }

    @GetMapping("/admin/activate/{username}")
    public String processUserActivation(@PathVariable(name = "username") String username) {
        userService.activateUser(username);
        return "redirect:/user/admin/users";
    }

    @GetMapping("/admin/deactivate/{username}")
    public String processUserDeactivation(@PathVariable(name = "username") String username) {
        userService.deactivateUser(username);
        return "redirect:/user/admin/users";
    }
}
