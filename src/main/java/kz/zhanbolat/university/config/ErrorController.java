package kz.zhanbolat.university.config;

import kz.zhanbolat.university.config.exception.InternalServerException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler(value = InternalServerException.class)
    public String processInternalServerException(Exception e) {
        return "error";
    }
}
