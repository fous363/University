package kz.zhanbolat.university.schedule.boundary.impl;

import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.schedule.boundary.ClassesScheduleService;
import kz.zhanbolat.university.schedule.control.ClassesScheduleRepository;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.semester.entity.Semester;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClassesScheduleServiceImpl implements ClassesScheduleService {
    @Autowired
    private ClassesScheduleRepository scheduleRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private StudentService studentService;

    @Override
    public void createUserSchedule(ClassesSchedule schedule, Integer userId) {
        if (scheduleRepository.isUserDateTimeEngaged(schedule, userId)) {
            throw new IllegalArgumentException();
        }
        scheduleRepository.createUserSchedule(schedule, userId);
    }

    @Override
    public List<ClassesSchedule> getStudentUserClassesSchedule(Integer userId) {
        return scheduleRepository.findAllByStudentUserId(userId);
    }

    @Override
    public List<ClassesSchedule> getLecturerUserClassesSchedule(Integer userId) {
        return scheduleRepository.findAllByLecturerUserId(userId);
    }

    @Override
    public void createGroupSchedule(ClassesSchedule schedule, Integer groupId) {
        List<Student> students = studentService.getAllStudents(groupId);
        students.forEach(student -> createUserSchedule(schedule, student.getUser().getId()));
    }

    @Override
    public ClassesSchedule getSchedule(Integer scheduleId) {
        return scheduleRepository.findOne(scheduleId);
    }

    @Override
    public void markStudent(Student student, ClassesSchedule schedule, Integer mark) {
        scheduleRepository.updateScheduleMark(student, schedule, mark);
    }
}
