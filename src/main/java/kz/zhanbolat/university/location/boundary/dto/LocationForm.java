package kz.zhanbolat.university.location.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class LocationForm {
    private Integer id;
    private String audience;
    private String campus;

    public LocationForm() {
    }

    public LocationForm(Integer id, String audience, String campus) {
        this.id = id;
        this.audience = audience;
        this.campus = campus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("audience", audience)
                .append("campus", campus)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LocationForm that = (LocationForm) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(audience, that.audience)
                .append(campus, that.campus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(audience)
                .append(campus)
                .toHashCode();
    }
}
