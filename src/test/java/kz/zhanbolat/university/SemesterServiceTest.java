package kz.zhanbolat.university;

import kz.zhanbolat.university.config.TestDBConfiguration;
import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.semester.boundary.impl.SemesterServiceImpl;
import kz.zhanbolat.university.semester.control.impl.SemesterRepositoryImpl;
import kz.zhanbolat.university.semester.entity.Semester;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringJUnitConfig(classes = {TestDBConfiguration.class, SemesterRepositoryImpl.class, SemesterServiceImpl.class})
@DirtiesContext
public class SemesterServiceTest {
    @Autowired
    private SemesterService semesterService;

    @Test
    public void check_getAllStudentSemesters_ReturnSemesters() {
        List<Semester> semesters = semesterService.getAllStudentSemesters(1);

        assertNotNull(semesters);
        assertEquals(1, semesters.size());
    }
}
