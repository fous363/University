package kz.zhanbolat.university.group.boundary.mapper;

import kz.zhanbolat.university.group.boundary.dto.GroupForm;
import kz.zhanbolat.university.group.entity.Group;

public class GroupMapper {

    public static GroupForm map(Group source) {
        return new GroupForm(source.getId(), source.getCode(), source.getName());
    }

    public static Group map(GroupForm source) {
        return new Group(source.getId(), source.getCode(), source.getName());
    }
}
