package kz.zhanbolat.university.course.boundary.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CourseForm {
    private String id;
    private String code;
    private String name;
    private String description;

    public CourseForm() {

    }

    public CourseForm(Builder builder) {
        this.id = builder.id;
        this.code = builder.code;
        this.name = builder.name;
        this.description = builder.description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String id;
        private String code;
        private String name;
        private String description;

        private Builder() {

        }

        public Builder id(String id) {
            this.id = id;

            return this;
        }

        public Builder code(String code) {
            this.code = code;

            return this;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder description(String description) {
            this.description = description;

            return this;
        }

        public CourseForm build() {
            return new CourseForm(this);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("code", code)
                .append("name", name)
                .append("description", description)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CourseForm that = (CourseForm) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(code, that.code)
                .append(name, that.name)
                .append(description, that.description)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(code)
                .append(name)
                .append(description)
                .toHashCode();
    }
}
