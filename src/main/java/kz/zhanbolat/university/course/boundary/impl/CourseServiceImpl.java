package kz.zhanbolat.university.course.boundary.impl;

import com.google.common.base.Strings;
import kz.zhanbolat.university.course.boundary.CourseService;
import kz.zhanbolat.university.course.control.CourseRepository;
import kz.zhanbolat.university.course.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
//TODO: ADD TESTS CASES
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> getAllUserCourses(String username) {
        return courseRepository.findAllByUsername(username);
    }

    @Override
    public void createCourse(Course course) {
        if (courseRepository.isCourseExisted(course.getCode()) || course.getCode().length() > 10) {
            throw new IllegalArgumentException("Invalid course to create");
        }
        courseRepository.create(course);
    }

    @Override
    public void deleteCourse(String courseCode) {
        courseRepository.delete(courseCode);
    }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourse(String courseCode) {
        return courseRepository.findOne(courseCode);
    }

    @Override
    public void updateCourse(Course course) {
        if (Strings.isNullOrEmpty(course.getCode()) || Strings.isNullOrEmpty(course.getName())) {
            throw new IllegalStateException();
        }
        courseRepository.update(course);
    }
}
