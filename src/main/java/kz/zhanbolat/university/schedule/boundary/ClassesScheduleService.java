package kz.zhanbolat.university.schedule.boundary;

import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.entity.Student;

import java.util.List;

public interface ClassesScheduleService {
    void createUserSchedule(ClassesSchedule schedule, Integer userId);
    List<ClassesSchedule> getStudentUserClassesSchedule(Integer userId);
    List<ClassesSchedule> getLecturerUserClassesSchedule(Integer userId);
    void createGroupSchedule(ClassesSchedule schedule, Integer groupId);
    ClassesSchedule getSchedule(Integer scheduleId);
    void markStudent(Student student, ClassesSchedule schedule, Integer mark);
}
