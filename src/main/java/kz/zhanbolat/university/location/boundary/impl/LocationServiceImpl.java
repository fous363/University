package kz.zhanbolat.university.location.boundary.impl;

import kz.zhanbolat.university.location.boundary.LocationService;
import kz.zhanbolat.university.location.control.LocationRepository;
import kz.zhanbolat.university.location.entity.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {
    @Autowired
    private LocationRepository locationRepository;

    @Override
    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    @Override
    public Location getLocation(Integer locationId) {
        return locationRepository.findOne(locationId);
    }

    @Override
    public void createLocation(Location location) {
        if (locationRepository.checkExists(location)) {
            throw new IllegalArgumentException("Location already exists");
        }
        locationRepository.create(location);
    }

    @Override
    public void updateLocation(Location location) {
        if (locationRepository.checkExists(location)) {
            throw new IllegalArgumentException("Location already exists");
        }
        locationRepository.update(location);
    }

    @Override
    public void deleteLocation(Integer locationId) {
        locationRepository.delete(locationId);
    }

    @Override
    public Location getLocation(String audience, String campus) {
        return locationRepository.findOneByAudienceAndCampus(audience, campus);
    }
}
