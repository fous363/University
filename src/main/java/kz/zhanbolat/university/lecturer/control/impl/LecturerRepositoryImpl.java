package kz.zhanbolat.university.lecturer.control.impl;

import kz.zhanbolat.university.lecturer.control.LecturerRepository;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Date;
import java.util.Objects;

@Repository
public class LecturerRepositoryImpl implements LecturerRepository {
    private static final String SELECT_ONE_BY_USERNAME = "select lecturer.id, lecturer.fio, lecturer.birthday, " +
            "lecturer.phone, lecturer.address, lecturer.email, lecturer.academic_degree_name, lecturer.user_id " +
            "from lecturer inner join user_account on user_account.id = lecturer.user_id " +
            "where user_account.username=:username";
    private static final String UPDATE_LECTURER_INFO_BY_USERNAME = "UPDATE lecturer " +
            "SET fio=:fio, birthday=:birthday, phone=:phone, email=:email, address=:address, " +
            "academic_degree_name=:academicDegree FROM user_account WHERE user_account.username=:username";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void create(Lecturer lecturer) {
        entityManager.persist(lecturer);
    }

    @Override
    public Lecturer findOneByUsername(String username) {
        Query query = entityManager.createNativeQuery(SELECT_ONE_BY_USERNAME, Lecturer.class);
        query.setParameter("username", username);
        return (Lecturer) query.getSingleResult();
    }

    @Override
    public void updateLecturerInfoByUsername(Lecturer lecturer, String username) {
        Query query = entityManager.createNativeQuery(UPDATE_LECTURER_INFO_BY_USERNAME);
        query.setParameter("fio", lecturer.getFio());
        query.setParameter("birthday", Date.valueOf(lecturer.getBirthday()));
        query.setParameter("phone", lecturer.getPhone());
        query.setParameter("email", lecturer.getEmail());
        query.setParameter("address", lecturer.getAddress());
        query.setParameter("academicDegree", lecturer.getAcademicDegree().getName());
        query.setParameter("username", username);
        query.executeUpdate();
    }
}
