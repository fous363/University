package kz.zhanbolat.university.lecturer.boundary;

import kz.zhanbolat.university.lecturer.entity.Lecturer;

public interface LecturerService {
    void registerNewLecturer(Lecturer lecturer);
    Lecturer getLecturerByUsername(String username);
    void updateLecturerInfo(Lecturer lecturer, String username);
}
