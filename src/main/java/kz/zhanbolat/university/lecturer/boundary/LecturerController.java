package kz.zhanbolat.university.lecturer.boundary;

import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.course.boundary.CourseService;
import kz.zhanbolat.university.course.entity.Course;
import kz.zhanbolat.university.degree.boundary.AcademicDegreeService;
import kz.zhanbolat.university.degree.entity.AcademicDegree;
import kz.zhanbolat.university.group.boundary.GroupService;
import kz.zhanbolat.university.group.entity.Group;
import kz.zhanbolat.university.lecturer.boundary.dto.LecturerForm;
import kz.zhanbolat.university.lecturer.boundary.mapper.LecturerMapper;
import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.semester.boundary.SemesterService;
import kz.zhanbolat.university.semester.entity.Semester;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.entity.Student;
import kz.zhanbolat.university.user.boundary.UserService;
import kz.zhanbolat.university.user.entity.User;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/lecturer")
public class LecturerController {
    @Autowired
    private LecturerService lecturerService;
    @Autowired
    private AcademicDegreeService academicDegreeService;
    @Autowired
    private UserService userService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private GroupService groupService;

    @ModelAttribute("lecturer")
    public LecturerForm lecturer() {
        return new LecturerForm();
    }

    @GetMapping("/singUp")
    public String singUpPage(ModelMap model) {
        model.addAttribute("academicDegrees", academicDegreeService.getAllAcademicDegrees());
        return "lecturerSingUp";
    }

    @PostMapping("/singUp")
    public String processSingUp(LecturerForm lecturerForm, HttpSession session) {
        String username = (String) session.getAttribute("username");
        Lecturer lecturer = LecturerMapper.map(lecturerForm);
        User user = userService.findUserByUsername(username);
        AcademicDegree academicDegree = academicDegreeService.getAcademicDegree(lecturerForm.getAcademicDegreeName());
        lecturer.setUser(user);
        lecturer.setAcademicDegree(academicDegree);
        lecturerService.registerNewLecturer(lecturer);
        session.removeAttribute("username");
        return "redirect:/user/login";
    }

    @GetMapping("/profile")
    public String profilePage(ModelMap modelMap, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        Lecturer lecturer = lecturerService.getLecturerByUsername(username);
        modelMap.addAttribute("lecturer", LecturerMapper.map(lecturer));
        return "lecturerProfile";
    }

    @GetMapping("/profile/update")
    public String profileUpdatePage(ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        Lecturer lecturer = lecturerService.getLecturerByUsername(username);
        model.addAttribute("lecturer", LecturerMapper.map(lecturer));
        model.addAttribute("academicDegrees", academicDegreeService.getAllAcademicDegrees());
        return "lecturerProfileUpdate";
    }

    @PostMapping("/profile/update")
    public String processProfileUpdate(LecturerForm lecturerForm, HttpSession session) {
        Lecturer lecturer = LecturerMapper.map(lecturerForm);
        AcademicDegree academicDegree = academicDegreeService.getAcademicDegree(lecturerForm.getAcademicDegreeName());
        lecturer.setAcademicDegree(academicDegree);
        lecturerService.updateLecturerInfo(lecturer,
                UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token")));
        return "redirect:/lecturer/profile";
    }

    @GetMapping("/students")
    public String lecturerStudentsPage(ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        List<Student> students = studentService.getAllStudents(username);
        model.addAttribute("students", students);
        return "lecturerStudents";
    }

    @GetMapping("/groups")
    public String lecturerGroupsPage(ModelMap model, HttpSession session) {
        String username = UserAuthorityTable.INSTANCE.getUsername((String) session.getAttribute("access-token"));
        List<Group> groups = groupService.getAllGroups(lecturerService.getLecturerByUsername(username).getId());
        model.addAttribute("groups", groups);
        return "lecturerGroups";
    }
}
