package kz.zhanbolat.university.schedule.control;

import kz.zhanbolat.university.lecturer.entity.Lecturer;
import kz.zhanbolat.university.schedule.entity.ClassesSchedule;
import kz.zhanbolat.university.student.entity.Student;

import java.util.List;

public interface ClassesScheduleRepository {
    void createUserSchedule(ClassesSchedule schedule, Integer userId);
    boolean isUserDateTimeEngaged(ClassesSchedule schedule, Integer userId);
    List<ClassesSchedule> findAllByLecturerUserId(Integer userId);
    List<ClassesSchedule> findAllByStudentUserId(Integer userId);
    ClassesSchedule findOne(Integer scheduleId);
    void updateScheduleMark(Student student, ClassesSchedule schedule, Integer mark);
}
