package kz.zhanbolat.university.user.control.impl;

import kz.zhanbolat.university.user.control.TokenGenerator;
import kz.zhanbolat.university.user.control.security.MessageEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TokenGeneratorImpl implements TokenGenerator {
    @Autowired
    private MessageEncryptor encryptor;

    @Override
    public String generateToken(String... credentials) {
        StringBuilder builder = new StringBuilder();
        for (String credential : credentials) {
            builder.append(credential);
        }
        return encryptor.encryptMessage(builder.toString());
    }
}
