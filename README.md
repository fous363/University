University
====
This is a project for my finals

Technologies
===
* Language: Java
* Frameworks: Spring, Hibernate, JUnit 5
* CI/CD: GitLab CI/CD
* DBMS: Postgresql
* Build system: Maven
