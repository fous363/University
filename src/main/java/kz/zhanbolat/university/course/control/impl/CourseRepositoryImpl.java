package kz.zhanbolat.university.course.control.impl;

import kz.zhanbolat.university.course.control.CourseRepository;
import kz.zhanbolat.university.course.entity.Course;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@Repository
public class CourseRepositoryImpl implements CourseRepository {
    private static final String SELECT_ALL_BY_USERNAME = "select course.id, course.code, course.name, course.description from course " +
            "inner join user_course on user_course.course_id = course.id " +
            "inner join user_account on user_account.id = user_course.user_id " +
            "where user_account.username=:username";
    private static final String DELETE_COURSE = "delete from course where code=:course_code";
    private static final String SELECT_ALL = "select id, code, name, description from course";
    private static final String SELECT_ONE_BY_CODE = "select id, code, name, description from course where code=:code";
    private static final String COUNT_COURSE_BY_CODE = "select count(*) from course where code = :code";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Course> findAllByUsername(String username) {
        final Query query = entityManager.createNativeQuery(SELECT_ALL_BY_USERNAME, Course.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    @Override
    public void create(Course course) {
        entityManager.persist(course);
    }

    @Override
    public boolean isCourseExisted(String code) {
        final Query query = entityManager.createNativeQuery(COUNT_COURSE_BY_CODE);
        query.setParameter("code", code);
        return ((BigInteger) query.getSingleResult()).intValue() > 0;
    }

    @Override
    public void delete(String courseCode) {
        Query query = entityManager.createNativeQuery(DELETE_COURSE);
        query.setParameter("course_code", courseCode);
        query.executeUpdate();
    }

    @Override
    public List<Course> findAll() {
        Query query = entityManager.createNativeQuery(SELECT_ALL, Course.class);
        return query.getResultList();
    }

    @Override
    public Course findOne(String courseCode) {
        final Query query = entityManager.createNativeQuery(SELECT_ONE_BY_CODE, Course.class);
        query.setParameter("code", courseCode);
        return (Course) query.getSingleResult();
    }

    @Override
    public void update(Course course) {
        entityManager.merge(course);
    }
}
