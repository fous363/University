package kz.zhanbolat.university.semester.boundary;

import kz.zhanbolat.university.config.UserAuthorityTable;
import kz.zhanbolat.university.course.boundary.dto.CourseForm;
import kz.zhanbolat.university.semester.boundary.dto.SemesterForm;
import kz.zhanbolat.university.semester.boundary.mapper.SemesterMapper;
import kz.zhanbolat.university.semester.entity.Semester;
import kz.zhanbolat.university.student.boundary.StudentService;
import kz.zhanbolat.university.student.entity.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/semester")
public class SemesterController {
    private static final Logger logger = LogManager.getLogger(SemesterController.class);
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private StudentService studentService;

    @GetMapping("/student")
    public String getAllStudentSemester(HttpSession session, ModelMap model) {
        String token = (String) session.getAttribute("access-token");
        String username = UserAuthorityTable.INSTANCE.getUsername(token);
        Student student = studentService.getStudentByUsername(username);
        List<SemesterForm> semesters = semesterService.getAllStudentSemesters(student.getId())
                .stream().map(SemesterMapper::map).collect(Collectors.toList());
        model.addAttribute("semesters", semesters);
        return "studentSemesters";
    }

    @GetMapping("/admin")
    public String adminSemesterPage(ModelMap model) {
        List<Semester>semesters = semesterService.getAllSemesters();
        model.addAttribute("semesters",
                semesters.stream().map(SemesterMapper::map).collect(Collectors.toList()));
        return "adminSemester";
    }

    @GetMapping("/admin/create")
    public String adminCreateSemesterPage(ModelMap model) {
        model.addAttribute("semester", new SemesterForm());
        return "adminCreateSemester";
    }

    @PostMapping("/admin/create")
    public String processSemesterCreation(@ModelAttribute(name = "semester") SemesterForm semesterForm, ModelMap model) {
        try {
            semesterService.createSemester(SemesterMapper.map(semesterForm));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminCreateSemesterPage(model);
        }
        return "redirect:/semester/admin";
    }

    @GetMapping("/admin/delete/{semester_id}")
    public String processDeleteSemester(@PathVariable("semester_id") Integer semesterId) {
        semesterService.deleteSemester(semesterId);
        return "redirect:/semester/admin";
    }

    @GetMapping("/admin/update/{semester_id}")
    public String adminUpdateSemesterPage(@PathVariable("semester_id") Integer semesterId, ModelMap model) {
        model.addAttribute("semester", SemesterMapper.map(semesterService.getSemester(semesterId)));
        return "adminUpdateSemester";
    }

    @PostMapping("/admin/update")
    public String processUpdateSemester(@ModelAttribute(name = "semester") SemesterForm semester, ModelMap model) {
        try {
            semesterService.updateSemester(SemesterMapper.map(semester));
        } catch (IllegalArgumentException e) {
            logger.error("Log error {0}", e);
            model.addAttribute("error", e.getMessage());
            return adminUpdateSemesterPage(semester.getId(), model);
        }
        return "redirect:/semester/admin";
    }
}
